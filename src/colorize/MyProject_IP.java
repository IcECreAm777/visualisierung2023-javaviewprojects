package colorize;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Info panel of tutorial project. Each project, as well as each geometry, may have
 * an info panel for inspecting and steering the instance. Modifications in the panel
 * should result in a call to <code>project.update(Object)</code>.
 * <p>
 * Panel adds simple sliders for modifying integer values of the project. The sliders
 * are children of the project and send <code>update(Object)</code> whenever they change
 * by user interaction.
 * <p>
 * This info panel is optional and can be removed without any changes in the project.
 * Each info panel must follow special naming conventions such that JavaView is able to
 * show it automatically, or on request. The class name must be identical to the class
 * name of the corresponding class with the suffix '_IP' attached. IP means InfoPanel.
 * 
 * @see			jv.project.PjProject
 * @author		Konrad Polthier
 * @version		23.10.16, 1.60 revised (kp) Doc and notice added.<br>
 *					28.11.00, 1.50 revised (kp) Applet simplified for beginners.<br>
 *					20.03.00, 1.10 revised (kp) Sliders moved to project.<br>
 *					05.08.99, 1.00 created (kp)
 */
public class MyProject_IP extends PjProject_IP implements ActionListener {
	protected	MyProject		myProject;
	protected	Panel				sliderPanel;
	protected	Label				lArea;
	protected	Button			bReset;
	protected Button colorizeButton;

	/** Constructor */
	public MyProject_IP() {
		super();
		if (getClass() == MyProject_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		// Add title label which will later display the title of the project.
		addTitle("");
		addNotice(getNotice());

		// Panel to contain sliders of u-lines and v-lines.
		sliderPanel = new Panel();
		sliderPanel.setLayout(new GridLayout(2, 1));
		add(sliderPanel);

		// Panel with label to display the area of the current surface.
		Panel pArea = new Panel();
		pArea.setLayout(new GridLayout(1, 2));
		pArea.add(new Label("Surface Area"));
		lArea = new Label();
		pArea.add(lArea);
		add(pArea);
		
		// Add reset button at bottom
		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		bReset = new Button("Reset");
		bReset.addActionListener(this);
		buttonPanel.add(bReset);
		add(buttonPanel);

		// add the colorize button
		var colorizePanel = new Panel();
		colorizePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		colorizeButton = new Button("Colorize");
		colorizeButton.addActionListener(this);
		colorizePanel.add(colorizeButton);
		add(colorizePanel);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "My first project with info panel. This notice information is optional "+
							 "for each project. Here a morphing animation between two minimal surfaces "+
							 "is shown and can be controlled by an animation dialog.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		myProject = (MyProject)parent;
		// Add info panels of integers to panel of sliders.
		sliderPanel.add(myProject.m_numULines.getInfoPanel());
		sliderPanel.add(myProject.m_numVLines.getInfoPanel());
	}
	/**
	* Here we arrive from outside world of this panel, e.g. if
	* project has changed somewhere else and must update its panel. Such an update
	* is automatically by superclasses of PjProject.
	*/
	public boolean update(Object event) {
		if (myProject==null) {
			if (PsDebug.WARNING) PsDebug.warning("missing parent, setParent not called");
			return false;
		}
		if (event == myProject) {
			lArea.setText(String.valueOf(myProject.m_geom.getArea()));
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (myProject==null)
			return;
		Object source = event.getSource();
		if (source == bReset) {
			myProject.init();
			myProject.update(this);
		}

		if(source == colorizeButton) {
			for (int i = 0; i < myProject.m_geom.getNumElements(); i++) {
				var color = i % 2 == 0 ? Color.BLUE : Color.white;
				myProject.m_geom.setElementColor(i, color);
				myProject.m_geom.setElementBackColor(i, color);
			}
		}
	}
}

