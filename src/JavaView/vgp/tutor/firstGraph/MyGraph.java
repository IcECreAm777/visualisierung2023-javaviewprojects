package vgp.tutor.firstGraph;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Rectangle;

import jv.geom.PgElementSet;
import jv.object.PsConfig;
import jv.object.PsMainFrame;
import jv.project.PvDisplayIf;
import jv.viewer.PvDisplay;

/**
 * Demo applet shows own data as a graph over the x-y plane.
 * Own data is given as an array of z-values which determine
 * the height of each vertex.
 * <p>
 * Applet uses no viewer manager, nor tool bar or status bar.
 * 
 * @author		Konrad Polthier
 * @version		31.03.20, 1.10 revised (kp) Assure display height when tool bar and monitor scale are active.<br>
 *					29.12.01, 1.00 created (kp)
 */
public class MyGraph extends Applet {
	/** Default display showing the geometries. */
	protected 	PvDisplayIf		m_disp;
	/** Frame if run standalone, null if run as applet */
	public		Frame				m_frame			= null;
	/**
	 * Applet support. Configure and initialize the viewer,
	 * load geometry and add display.
	 */
	public void init() {
		// JavaView's configuration class needs to know if running as applet or application.
		PsConfig.init(this, m_frame);
		
		// Create viewer for viewing 3d geometries, and register applet.
		m_disp = new PvDisplay();

		// Create a simple geometry. PgElementSet is the base class
		// for surfaces. See jv.geom.* for more geometry classes.
		PgElementSet geom = new PgElementSet(3);
		geom.setName("Sin-Cos Graph");
		// Compute coordinates a graph over the x-y plane.
		// Set number of lines in x and y directions.
		int numXLines = 8;
		int numYLines = 5;

		// Allocate space for vertices.
		geom.setNumVertices(numXLines*numYLines);
		// Compute the vertices.
		int ind = 0;
		for (int i=0; i<numXLines; i++) {
			double x = Math.PI*i/(numXLines-1);
			for (int j=0; j<numYLines; j++) {
				double y = Math.PI*j/(numYLines-1);
				double z = Math.sin(x)*Math.cos(y);
				geom.setVertex(ind++, x, y, z);
			}
		}

		// Compute connectivity of a rectangular mesh.
		geom.makeQuadrConn(numXLines, numYLines);
		
		// Why not, just assign some colors.
		geom.makeElementColorsFromXYZ();
		geom.showElementColors(true);
		
		// Register the geometry in the display, and make it active.
		m_disp.addGeometry(geom);
		// The selected geometry receives pick events. Usually one needs
		// to select a geometry only if more geometries are registered
		// in a display.
		m_disp.selectGeometry(geom);

		// Standard Java technique to add a component to an applet.
		setLayout(new BorderLayout());
		add(m_disp.getCanvas(), BorderLayout.CENTER);
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		MyGraph myGraph	= new MyGraph();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(myGraph, args);
		frame.pack();
		myGraph.m_frame	= frame;
		myGraph.init();

		// Slight sleeping helps to get the correct initial size of the display.
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {}
		
		// Setting the bound no longer works correctly since the tool bar and status bar were added.
		// Therefore we validate again at the end of the run() method.
		int otherHeight	= PsMainFrame.getTotalHeightOther(frame, myGraph);

		// Position of left upper corner and size of frame when run as application.
		frame.setInnerBounds(new Rectangle(420, 5, 640, 512 + otherHeight/PsConfig.getMonitorScale()));
		frame.setVisible(true);
	}
}
