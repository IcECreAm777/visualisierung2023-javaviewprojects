package vgp.tutor.key;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PgGeometry;
import jv.project.PjProject_IP;

/**
 * Info panel of keyframe animation of precomputed models.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 1.50 revised (kp) Doc and notices added.<br>
 *					00.00.99, 1.00 created (kp)
 */
public class PjKeyframe_IP extends PjProject_IP {
	protected	PgGeometry				m_geom;

	/** Constructor */
	public PjKeyframe_IP() {
		super();
		if (getClass() == PjKeyframe_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Demo project showing a simple Keyframe. "+
							 "Press F4 or Strg-A to display the Keyframe panel "+
							 "and press the START button in the panel. "+
							 "While geometry animates, interaction in the viewer "+
							 "is simultaneously possible.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_geom = (PgGeometry)m_project.getGeometry();
	}
	/**
	* Here we arrive from outside world of this panel, e.g. if
	* parent has changed somewhere else.
	*/
	public boolean update(Object event) {
		if (m_project==null) {
			if (PsDebug.WARNING) PsDebug.warning("missing parent, setParent not called");
			return false;
		}
		if (event == m_project) {
			m_geom = (PgGeometry)m_project.getGeometry();
			if (m_geom != null)
				setTitle("Keyframe of "+m_geom.getName());
			return true;
		}
		return super.update(event);
	}
}

