package vgp.tutor.key;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet demonstrates the use of key frames to view an animated sequence of geometry files.
 * 
 * @see			vgp.tutor.key.PjKeyframe
 * @author		Konrad Polthier
 * @version		15.12.16, 3.00 revised (kp) Body moved to PjKeyframe.<br>
 * 				08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				23.10.99, 1.10 revised (kp) Argument of project removed.<br>
 *					19.07.99, 1.01 revised (kp) Start a thread during initialization.<br>
 *					30.06.99, 1.00 created (kp)
 */
public class PaKeyframe extends PvApplet {
	/**
	 * Interface used by design tools to show properties of applet.
	 * This method returns a list of string arrays, each of length 4 rather than 3
	 * as suggest by Java. The additional string at third position contains
	 * the value of the parameter.
	 * @see			jv.viewer.PvViewer#getParameter(String)
	 */
	public String[][] getParameterInfo() { return PjKeyframe.m_parmKeyframe; }

	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Demo applet showing a keyframe animation." + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjKeyframe();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaKeyframe(), args);
	}
}
