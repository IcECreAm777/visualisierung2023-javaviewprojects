package vgp.tutor.parmCurve;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet for Parametrized Curves.
 * 
 * @see			vgp.tutor.parmCurve.PjParmCurve
 * @author		Eike Preuss
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				11.06.02, 1.00 created (ep)
 */
public class PaParmCurve extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Eike Preuss" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Applet displays parametrized curves in JavaView" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjParmCurve();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaParmCurve(), args);
	}
}
