package vgp.tutor.parmCurve;

import jv.project.PjProject;
import jvx.curve.PgParmCurve;

/**
 * Study parametrized curves and interactively edit coordinate functions.
 *
 * @see			jvx.curve.PgParmCurve
 * @author		Eike Preuss
 * @version		04.11.02, 1.01 revised (ep) Changed name of curve.<br>
 *					11.06.02, 1.00 created (ep)
 */
public class PjParmCurve extends PjProject {
	/** Parametrized curve. */
	protected	PgParmCurve				m_parmCurve;

	/** Constructor */
	public PjParmCurve() {
		super("Parametrized Curve Demo");
		m_parmCurve		= new PgParmCurve(3);
		m_parmCurve.setName("Curve");
		m_parmCurve.setParent(this);
		if (getClass() == PjParmCurve.class) {
			init();
		}
	}
	public void init() {
		super.init();
		m_parmCurve.init();
	}
	public void start() {
		m_parmCurve.computeCurve();
		addGeometry(m_parmCurve);
		selectGeometry(m_parmCurve);
		super.start();
	}

	/**
	 * Update the class whenever a child has changed.
	 * Method is usually invoked from the children.
	 */
	public boolean update(Object event) {
		if (event == m_parmCurve) {
			return super.update(null);
		}
		return super.update(event);
	}
}
