package vgp.tutor.parmCurve;

import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for editing coordinate functions of parametrized curve.
 * 
 * @see			jvx.curve.PgParmCurve_CP
 * @author		Eike Preuss
 * @version		23.10.16, 1.10 revised (kp) Doc and notice added.<br>
 *					11.06.02, 1.00 created (ep)
 */
public class PjParmCurve_IP extends PjProject_IP {
	protected	PjParmCurve			m_pjParmCurve;

	/** Constructor */
	public PjParmCurve_IP() {
		super();
		if (getClass() == PjParmCurve_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
	}		
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Study parametrized curves and interactively edit coordinate functions.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjParmCurve = (PjParmCurve)parent;
		// Create an info panel of the workshop by ourself
		PsPanel cp = m_pjParmCurve.m_parmCurve.newInspector(PsPanel.CONFIG_EXT);
		cp.setBorderType(PsPanel.BORDER_NONE);
		add(cp);
		validate();
	}
}
