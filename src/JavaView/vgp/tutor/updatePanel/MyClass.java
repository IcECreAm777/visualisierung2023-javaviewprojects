package vgp.tutor.updatePanel;

import jv.object.*;

/**
 * Tutorial class for combined used of JavaView update mechanism and AWT 1.1 events.
 * <p>
 * Any subclass of PsObject may have an associated info panel for inspecting
 * itself in an interactive panel. The info panel may either be supplied by a
 * superclass, or by providing a panel whose class name has appended '_IP' to
 * the name of the class which shall be inspected. In this tutorial, the base
 * class is MyClass and its panel is MyClass_IP. Invoking the method
 * <code>getInfoPanel()</code> returns an instance of the panel, as done in
 * the <code>main(String [])</code>, which can be inserted in a frame or in
 * other panels. In JavaView, the panel is usually added to the control window. 
 * <p>
 * The base class and its panel communicate via the methods <code>update(Object)</code>
 * once the base class is registered in the panel as its parent. This registration
 * is done in the super class PsObject after the first call of <code>getInfoPanel()</code>.
 * <p>
 * Base class and its panel separate the data and the widget user interface. The base
 * class contains the data and is usually embedded in a program, while the panel
 * contains the widget representation. The panel is just attached to the base class
 * and only accessible through it.
 * <p>
 * If multiple instances of an info panel are needed then one should use the
 * method <code>newInstance(PsPanel.INFO_EXT)</code> to obtain and register
 * new info panels. Advanced applications are advised to use this method.
 * 
 * @see			jv.object.PsObject
 * @see			jv.object.PsUpdateIf
 * @author		Konrad Polthier
 * @version		15.08.99, 1.00 revised (kp) <br>
 *					15.08.99, 1.00 created (kp)
 */
public class MyClass extends PsObject {
	/** Boolean flag. */
	protected	boolean		m_state;
	/** Entries of a list. */
	protected	String []	m_topics = {"Topic0", "Topic1", "Topic2", "Topic3", "Topic4"};
	/** Selected entry in list. */
	protected	int			m_selectedTopic;
	/** Text string. */
	protected	String		m_text;

	/**
	 * Constructor.
	 */
	public MyClass() {
		if (getClass() == MyClass.class) {
			init();
		}
	}
	/**
	 * Set values of variables. Method is also used to reset the class.
	 */
	public void init() {
		super.init();
		setName("My Class");
		m_state				= true;
		m_selectedTopic	= 0;
		m_text				= String.valueOf(m_selectedTopic);
	}

	/**
	 * Update the class whenever a child has changed.
	 * Method is usually invoked from the children.
	 */
	public boolean update(Object event) {
		if (event == getInfoPanel()) {
			return super.update(null);
		}
		return super.update(event);
	}
	
	public boolean getState() { return m_state; }
	public void setState(boolean flag) {
		m_state = flag;
	}
	
	public String [] getTopics() { return m_topics; }
	public int getSelectedTopic() { return m_selectedTopic; }
	public void setSelectedTopic(int ind) {
		m_selectedTopic = ind;
	}
	
	public String getText() { return m_text; }
	public void setText(String text) {
		m_text = text;
	}

	/**
	 * Standalone application support.
	 */
	public static void main(String args[]) {
		MyClass myClass = new MyClass();
		
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame("My Class", args);
		// Get 3d display from viewer and add it to applet
		frame.add("Center", myClass.getInfoPanel());
		frame.pack();
		// Position of left upper corner and size of frame when run as application.
		frame.setBounds(new java.awt.Rectangle(420, 5, 200, 400));
		frame.setVisible(true);
	}
}