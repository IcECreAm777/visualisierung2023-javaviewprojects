package vgp.tutor.avlTree;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * InfoPanel for AVL Tree project.
 *
 * @author		Ulrich Reitebuch
 * @version 	26.04.2013, 1.00 created (ur)
 */
public class PjAvlTree_IP extends PjProject_IP implements ActionListener{
	/** Parent project */
	protected PjAvlTree	m_pjTree;
	/** TextField for new node / delete node value. */
	protected TextField	m_tValue;
	/** Checkbox for Insert mode. */
	protected Checkbox	m_cInsert;
	/** Checkbox for Check Existence mode. */
	protected Checkbox	m_cCheck;
	/** Checkbox for Remove mode. */
	protected Checkbox	m_cRemove;
	
	/**
	 * Construct a new PjAperiodicTiling info panel.
	 */
	public PjAvlTree_IP() {
		super();
		if (getClass() == PjAvlTree_IP.class)
			init();
	}

	/**
	 * Initialize the info panel.
	 */
	public void init() {
		super.init();
		m_tValue = new TextField("", 40);
		m_tValue.setEditable(true);
		m_tValue.addActionListener(this);
		add(m_tValue);
		Panel bp = new Panel(new FlowLayout());
		{
			CheckboxGroup cbg = new CheckboxGroup();
			m_cInsert = new Checkbox("Insert", true, cbg);
			//m_cInsert.addItemListener(this);
			bp.add(m_cInsert);
			m_cRemove = new Checkbox("Remove", false, cbg);
			//m_cInsert.addItemListener(this);
			bp.add(m_cRemove);
			m_cCheck = new Checkbox("Check Existence", false, cbg);
			//m_cInsert.addItemListener(this);
			bp.add(m_cCheck);
		}
		add(bp);
		
	}

	/**
	 * Set the parent of this infopanel.
	 *
	 * @param		parent	The parent project.
	 */
	public void setParent(PsUpdateIf parent) {
		if (parent == null)
			return;
		if (!(parent instanceof PjAvlTree)) {
			if (PsDebug.WARNING) PsDebug.warning("Parent has to be of type PjAvlTree");
			return;
		}
		super.setParent(parent);
		m_pjTree = (PjAvlTree) parent;
	}

	/** React on pressed Button. */
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource(); 
		if (source == m_tValue) {
			if (m_cInsert.getState()) {
				m_pjTree.insert(m_tValue.getText());
			} else if (m_cRemove.getState()) {
				m_pjTree.remove(m_tValue.getText());
			} else {
				String test = (String)m_pjTree.m_tree.findNode(m_tValue.getText());
				if (test == null)
					PsDebug.message("Does not exist in the tree.");
				else
					PsDebug.message("Exists in the tree: "+test);
			}
		}		
	}
}
