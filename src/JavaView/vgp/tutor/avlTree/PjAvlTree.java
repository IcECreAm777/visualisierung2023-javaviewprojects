package vgp.tutor.avlTree;

import java.awt.Color;

import jv.geom.PgPolygonSet;
import jv.project.PjProject;
import jv.project.PvCameraIf;
import jv.vecmath.PdVector;

import jvx.util.PuAVLTree;
import jvx.util.PuBinaryTreeNode;
import jvx.util.PuCompareStringsLexi;

/**
 * Tutorial project for testing AVL Tree.
 *
 * @author		Ulrich Reitebuch
 * @version 	08.11.16, 1.10 revised (kp) Code tuned.<br>
 * 				26.04.13, 1.00 created (ur)
 */
public class PjAvlTree extends PjProject {
	/** Geometry to work on. */
	protected PgPolygonSet	m_geom;
	/** AVL Tree */
	protected PuAVLTree		m_tree;
	
	/**
	 * Construct a new Aperiodic Tiling project.
	 */
	public PjAvlTree() {
		super("AVL Tree Demo");

		m_geom = new PgPolygonSet(2);
		m_geom.setName("Tree");

		// Create tree!		
		m_tree = new PuAVLTree(new PuCompareStringsLexi());
		
		if (getClass() == PjAvlTree.class) {
			init();
		}
	}

	/**
	 * Initializes the project.
	 */
	public void init() {
		super.init();
		m_geom.showVertexLabels(true);
		m_geom.showVertexOutline(false);
		m_geom.setGlobalPolygonColor(new java.awt.Color(191, 191, 191));
		m_geom.setGlobalVertexSize(0.);
		m_geom.setLabelAttribute(jv.project.PvGeometryIf.GEOM_ITEM_POINT, 0, 0, jv.project.PgGeometryIf.LABEL_CENTER , jv.project.PgGeometryIf.LABEL_MIDDLE, 0);
	}
	/**
	 * Start project, e.g. start an animation. Method is called once when project is
	 * selected in PvViewer#selectProject(). Method is optional. For example, if an
	 * applet calls the start() method of PvViewer, then PvViewer tries to invoke
	 * the start() method of the currently selected project.
	 */
	public void start() {
		setEnabledAutoFit(true);
		if (m_display != null) {
			m_display.selectCamera(PvCameraIf.CAMERA_ORTHO_XY);
			m_display.setBackgroundColor(Color.white);
		}
	
		computeGeom();
		addGeometry(m_geom);
		selectGeometry(m_geom);

		super.start();
	}
	
	/** Insert a new node into the tree. */
	public void insert(String name) {
		m_tree.insert(name);
		computeGeom();
		m_geom.update(m_geom);
		if (isEnabledAutoFit()) {
			fitDisplays();
		}
	}
	/** Remove a node from the tree. */
	public void remove(String name) {
		m_tree.remove(name);
		computeGeom();		
		m_geom.update(m_geom);
		if (isEnabledAutoFit()) {
			fitDisplays();
		}
	}
	
	/** Compute tree geometry. **/
	public void computeGeom() {
		int nov = m_tree.getSize();
		if (nov == 0)
			return;
		m_geom.setNumVertices(nov);
		m_geom.setDimOfPolygons(2);
		m_geom.setNumPolygons(nov-1);
		// Traverse tree.
		double scaleX = 1.;
		double scaleY = 1.;
		PuBinaryTreeNode[] n = new PuBinaryTreeNode[nov];
		int height = m_tree.getHeight();
		if (height < 1)
			height = 1;
		n[0] = m_tree.getRoot();
		m_geom.getVertex(0).setName((String)(n[0].getValue()));
		m_geom.getVertex(0).set(0.0, 0.0);
		int nextFreeIndex = 1;
		for (int i=0; i<nov; i++) {
			// Add Children
			PdVector v = m_geom.getVertex(i);
			PuBinaryTreeNode right = n[i].getRight();
			if (right != null) {
				n[nextFreeIndex] = right;
				m_geom.getVertex(nextFreeIndex).set(v.m_data[0]+1, v.m_data[1]-Math.pow(0.5, v.m_data[0]+1));
				m_geom.getPolygon(nextFreeIndex-1).set(i, nextFreeIndex);
				m_geom.getVertex(nextFreeIndex).setName((String)(right.getValue()));
				nextFreeIndex++;
			}
			PuBinaryTreeNode left = n[i].getLeft();
			if (left != null) {
				n[nextFreeIndex] = left;
				m_geom.getVertex(nextFreeIndex).set(v.m_data[0]+1, v.m_data[1]+Math.pow(0.5, v.m_data[0]+1));
				m_geom.getPolygon(nextFreeIndex-1).set(i, nextFreeIndex);
				m_geom.getVertex(nextFreeIndex).setName((String)(left.getValue()));
				nextFreeIndex++;
			}
			// Scale vertex i
			v.m_data[0] *= scaleX/(1.*height);
			v.m_data[1] *= scaleY;
		}
	}
}

