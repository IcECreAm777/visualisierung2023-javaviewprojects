package vgp.tutor.avlTree;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Tutorial applet shows an AVL Tree. 
 *
 * @see			vgp.tutor.avlTree.PjAvlTree
 * @author		Ulrich Reitebuch
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				26.04.13, 1.00 created (ur)
 */
public class PaAvlTree extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Ulrich Reitebuch" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Tutorial applet showing an AVL Tree." + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjAvlTree();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaAvlTree(), args);
	}
}