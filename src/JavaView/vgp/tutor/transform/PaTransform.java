package vgp.tutor.transform;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Demo applet to use transformations in 4D ambient space forms.
 * 
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				09.06.06, 1.00 created (kp) Based on PaTransform applet.<br>
 */
public class PaTransform extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Demo applet to use transformations in ambient space forms." + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjTransform();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaTransform(), args);
	}
}
