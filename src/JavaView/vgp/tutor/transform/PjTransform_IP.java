package vgp.tutor.transform;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel of space form transformations with sliders.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 2.10 revised (kp) Doc and notice added.<br>
 *					09.06.06, 1.00 created (kp) Based on PjTransform_IP.<br>
 */
public class PjTransform_IP extends PjProject_IP implements ActionListener {
	protected	PjTransform	m_pjTransform;
	protected	PsPanel					m_pBounds;
	protected	Button					m_bReset;

	/** Constructor */
	public PjTransform_IP() {
		super();
		if (getClass() == PjTransform_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		m_pBounds = new PsPanel();
		addSubTitle("Rotation Angles");
		m_pBounds.setLayout(new GridLayout(3, 1));
		add(m_pBounds);

		// draw a separator
		addLine(1);

		// buttons at bottom
		Panel m_pBottomButtons = new Panel();
		m_pBottomButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		add(m_pBottomButtons);
		m_bReset = new Button("Reset");
		m_bReset.addActionListener(this);
		m_pBottomButtons.add(m_bReset);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Transformation of 4D geometries can be directly applied to "+
							 "the 4D vertices or, as shown here, by modifying the '4D ambient "+
							 "transformation matrix'. Optionally, each geometry may be equipped with a "+
							 "model matrix and an ambient matrix, being pre-multiplied to the "+
							 "current camera matrix. Note, the 4D coordinates of a geometry are "+
							 "shown in its inspector panel. The different matrices may also be "+
							 "modified interactively by putting the camera in respective modes, "+
							 "see the camera inspector for choices.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjTransform = (PjTransform)parent;
		m_pBounds.add(m_pjTransform.m_angleXW.newInspector(PsPanel.INFO_EXT));
		m_pBounds.add(m_pjTransform.m_angleYW.newInspector(PsPanel.INFO_EXT));
		m_pBounds.add(m_pjTransform.m_angleZW.newInspector(PsPanel.INFO_EXT));
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (m_pjTransform == event) {
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjTransform==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjTransform.init();
			m_pjTransform.m_surface.update(m_pjTransform.m_surface);
		}
	}
}

