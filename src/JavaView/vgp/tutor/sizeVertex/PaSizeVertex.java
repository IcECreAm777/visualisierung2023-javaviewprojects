package vgp.tutor.sizeVertex;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet shows usage of individual sizeVertex of vertices and polygon edges.
 * <p>
 * Changes the size of each vertex slightly back and forth with given random initial speed.
 * The overall speed of the system may be uniformly scaled by argument speed.
 * 
 * @see			vgp.tutor.sizeVertex.PjSizeVertex
 * @author		Konrad Polthier
 * @version		08.11.16, 3.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				04.11.06, 2.00 revised (kp) Moved to vgp.tutor.<br>
 *					07.02.04, 1.00 created (kp) 
 */
public class PaSizeVertex extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "3.00" + "\r\n" +
				 "Applet shows usage of individual sizeVertex of vertices and polygon edges" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjSizeVertex();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaSizeVertex(), args);
	}
}
