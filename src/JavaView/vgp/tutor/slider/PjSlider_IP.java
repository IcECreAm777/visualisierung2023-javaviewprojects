package vgp.tutor.slider;

import java.awt.*;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

import vgp.tutor.slider.PjSlider;

/**
 * Info panel of the slider demo.
 * @author		Ulrich Reitebuch, Konrad Polthier
 * @version		16.09.01, 1.50 revised (kp) Panel stuff moved to PjSlider_IP.<br>
 *					16.09.01, 1.10 revised (kp) Name changed from slider_IP.java.<br>
 *					14.04.00, 1.00 created (ur)
 */
public class PjSlider_IP extends PjProject_IP {
	/**
	 * The parent project contains all data information to be displayed
	 * in this panel. This parent is automatically assigned to this class
	 * via the method {@link #setParent(PsUpdateIf) setParent(PsUpdateIf)}. This panel is updated
	 * whenever the parent object changes via the method {@link #update(Object) update(Object)}.
	 * @see			#setParent(PsUpdateIf)
	 * @see			#update(Object)
	 */
	protected	PjSlider		m_pjSlider;
	/** Display numerical value of product of two sliders. */
	protected	Label			m_lProduct;
	/** Container of info panels of the two sliders. */
	protected	Panel			m_sliderPanel;
	
	/**Constructor*/
	public PjSlider_IP () {
		super();
		addTitle("");
		addNotice(getNotice());

		// Create panel to contain label and result of product.
		Panel pProduct	= new Panel(new GridLayout(1, 2));
		{
			// Create a descriptive label.
			Label label 	= new Label("Product of Slider Values: ");
			pProduct.add(label);
			// Another label will show the value of the product.
			m_lProduct 		= new Label();
			pProduct.add(m_lProduct);
		}
		add(pProduct);

		// Slider panel is a container for the info panels of the
		// two sliders.
		m_sliderPanel = new Panel(new GridLayout(2, 1));
		add(m_sliderPanel);

		if (getClass() == PjSlider_IP.class) {
			init();
		}
	}
	
	/** Initialization and/or reset this info panel. */
	public void init() {
		super.init();
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Demo class for the programming with sliders and their event handling. "+
							 "Here the product of two slider values is displayed in a text field. "+
							 "This demo does not use the display.";
		return notice;
	}
	/**
	 * Method assigns the data class to this panel class.
	 * This method is automatically called by JavaView whenever
	 * this panel is request from the data class via newInspector(PsPanel.INFO_EXT).
	 * @see			jv.object.PsObject#newInspector(String)
	 * @see			#update(Object)
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjSlider = (PjSlider)parent;
		m_sliderPanel.removeAll();
		m_sliderPanel.add(m_pjSlider.m_intSlider.newInspector(PsPanel.INFO_EXT));
		m_sliderPanel.add(m_pjSlider.m_doubleSlider.newInspector(PsPanel.INFO_EXT));
	}
	/**
	 * Method allows this panel to update whenever the data class has changed.
	 * This method is automatically called by JavaView whenever
	 * the data class is update via a call slider.update(sliderObj).
	 * @see			jv.object.PsPanel#setParent(PsUpdateIf)
	 */
	public boolean update(Object event) {
		if (event == null) {
			return false;
		}
		if (event == m_pjSlider) {
			setTitle(m_pjSlider.getName());
			m_lProduct.setText(String.valueOf(m_pjSlider.getProduct()));
			return true;
		}
		return super.update(event);
	}
}
