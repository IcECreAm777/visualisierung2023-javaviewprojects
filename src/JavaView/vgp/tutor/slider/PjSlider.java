package vgp.tutor.slider;

import jv.number.PuInteger;
import jv.number.PuDouble;
import jv.project.PjProject;

/**
 * Demo class for the usage of the sliders {@link jv.number.PuInteger jv.number.PuInteger} 
 * and {@link jv.number.PuDouble jv.number.PuDouble} in JavaView.
 * <p>
 * The sliders are the info panel of an integer respectively a double variable.
 * This sample project is a data class which uses the integer and double
 * objects as a inspectable numbers. That is, in this project the current
 * value of the numbers are obtain via their getValue and setValue method.
 * Simultaneously and independently, a GUI interface to these number may be
 * constructed, which is here done in the info panel of this project.
 * <p>
 * Note the handling of panels is encapsulated in the corresponding
 * class PjSlider_IP. I.e. this project class contains the values of the sliders,
 * but not the interface handling. Basically, the separation of interface and data
 * class allows that this class is fully functional even in batch operation.
 * 
 * @see			jv.number.PuDouble
 * @see			jv.number.PuInteger
 * @author		Ulrich Reitebuch, Konrad Polthier
 * @version		16.09.01, 1.50 revised (kp) Panel stuff moved to PjSlider_IP.<br>
 *					16.09.01, 1.10 revised (kp) Name changed from slider_Applet.java.<br>
 *					14.04.00, 1.00 created (ur)
 */
public class PjSlider extends PjProject {
	/** Slider with integer value. */
	protected	PuInteger		m_intSlider;
	/** Slider with floating point value. */
	protected	PuDouble			m_doubleSlider;
	/** Product of two values. */
	protected	double			m_product;

	/**
	 * Constructor: Creates instances of slider classes.
	 */
	public PjSlider (){
		super("Slider Demo");
		// Create an instance of "PuInteger" and an instance of "PuDouble".
		// The first argument for creating PuInteger or PuDouble is a string,
		// in this case "Integer" resp. "Double", which is the name of the object.
		// This name will appear inside the slider panel on the left side.
		// The second argument for creating PuInteger or PuDouble instances, in this case "this", has to fulfill 
		// the interface "PsUpdateIf" (in this case this is warranted by extending jv.project.PjProject),
		// particularly it must implement the method "public boolean update (Object)", which is called,
		// whenever the value of the slider panel is modified, or when an update
		// of the slider is performed.
		m_intSlider		= new PuInteger("Integer", this);
		m_doubleSlider	= new PuDouble("Double", this);
		init();
	}
	
	/**
	 * Initialization and configuration of sliders.
	 * At first, some default values of the two sliders are set.
	 * These default values are used for initialization of the slider
	 * and whenever the slider is reset.
	 */
	public void init() {
		// The method setDefBounds defines the minimum value, the maximum value, 
		// the line increment and the page increment for the slider.
		// The bounds and increments can also be changed later by the method PuInteger.setBounds
		// resp. PuDouble.setBounds.
		m_intSlider.setDefBounds(-10, 10, 1, 5);
		// The method setDefValue defines the initial value. The value can also be changed later
		// by the method setValue(Integer) resp. setValue(Double).
		m_intSlider.setDefValue(4);
		// The method PuInteger.init() resp. PuDouble.init() must be called to set the slider to it's
		// initial values.
		m_intSlider.init();
		
		// Now the same procedure with the double slider.
		m_doubleSlider.setDefBounds(-10., 10., 0.1, 1.);
		m_doubleSlider.setDefValue(2.3);
		m_doubleSlider.init();

		// Initialize the product variable.
		computeProduct();
	}
	/**
	 * This method allows to react whenever a slider is touched.
	 * Since this project is the parent of both sliders, this method
	 * is called whenever an update of a slider is invoked.
	 */
	public boolean update(Object event) {
		if (event==m_intSlider || event==m_doubleSlider) {
			// Update the product variable if the value of a slider has changed.
			computeProduct();
			// Update parents, dependent objects and the info panel of this class
			return super.update(this);
		}
		return super.update(event);
	}
	/**
	 * Get the current value of the product.
	 */
	public double getProduct() { return m_product; }
	/**
	 * Compute product of values of the two sliders.
	 */
	public void computeProduct () {
		//The methods PuInteger.getValue() and PuDouble.getValue() return the int/double
		//value defined by the slider.
		m_product = m_doubleSlider.getValue() * m_intSlider.getValue();
	}		
}
