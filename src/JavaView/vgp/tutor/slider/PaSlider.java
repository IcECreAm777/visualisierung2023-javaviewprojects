package vgp.tutor.slider;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Tutorial applet shows usage of double and integer sliders.
 * 
 * @see			vgp.tutor.slider.PjSlider
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				04.02.07, 1.20 revised (kp) Missing initialization of PsConfig added.<br>
 * 				16.09.01, 1.10 revised (kp) Name changed from slider_Applet.java according to naming convention.<br>
 */
public class PaSlider extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Ulrich Reitebuch" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Demo applet for usage of sliders for integers and doubles in JavaView" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjSlider();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaSlider(), args);
	}
}
