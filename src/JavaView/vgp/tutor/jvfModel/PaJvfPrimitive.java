package vgp.tutor.jvfModel;

import jv.object.PsViewerIf;
import jv.project.PvApplet;
import jv.project.PjProject;
import jv.project.PvCameraIf;
import jv.project.PvDisplayIf;
import jv.project.PvViewerIf;

import jvx.primitive.PgCircleF;
import jvx.primitive.PgCurveF;
import jvx.primitive.PgPlotF;
import jvx.primitive.PgPointF;
import jvx.primitive.PgSegmentF;

/**
 * WORK IN PROGRESS.
 * 
 * Demo applet for using functional JVF primitives.
 * 
 * @see			jvx.primitive
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 */
public class PaJvfPrimitive extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Demo applet for using functional JVF primitives." + "\r\n";
	}
	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void run() {
		super.run();

		// Create viewer for viewing 3d geometries
		PvViewerIf viewer		= super.getViewer();
		PvDisplayIf disp		= super.getDisplay();
		disp.selectCamera(PvCameraIf.CAMERA_ORTHO_XY);
		disp.setMajorMode(PvDisplayIf.MODE_VERTICAL);

		/*
		// Create and show various functional geometry classes.
		PgSphere sphere		= new PgSphere();
		sphere.showElements(false);
		sphere.setRadius(1.5);
		sphere.setGlobalEdgeColor(Color.white);
		sphere.compute();
		disp.addGeometry(sphere);
		
		PgCircle circle		= new PgCircle();
		circle.setNumVertices(50);
		circle.setRadius(2.5);
		circle.compute();
		disp.addGeometry(circle);

		PgGraphF graphF		= new PgGraphF();
		graphF.computeSample();
		graphF.getFunction().setExpression("sin(u*v)-2.");
		graphF.compute();
		graphF.setEnabledSynchronization(true);
		graphF.makeElementColorsFromZHue();
		graphF.showElementColors(true);
		graphF.update(graphF);
		disp.addGeometry(graphF);
		*/

		PgCircleF circleF		= new PgCircleF();
		circleF.computeSample();
		disp.addGeometry(circleF);

		PgPointF pointF		= new PgPointF();
		pointF.computeSample();
		disp.addGeometry(pointF);

		PgSegmentF segmentF	= new PgSegmentF();
		segmentF.computeSample();
		disp.addGeometry(segmentF);

		PgCurveF curveF		= new PgCurveF();
		curveF.computeSample();
		disp.addGeometry(curveF);

		PgPlotF plotF		= new PgPlotF();
		plotF.computeSample();
		disp.addGeometry(plotF);

		disp.selectGeometry(plotF);
		disp.fit();
		disp.update(disp);

		viewer.showPanel(PsViewerIf.CONFIG);
		viewer.showDialog(PsViewerIf.CONTROL);
	}
	/**
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return null;
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaJvfPrimitive(), args);
	}
}

