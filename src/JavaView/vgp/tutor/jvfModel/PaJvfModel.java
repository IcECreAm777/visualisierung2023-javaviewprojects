package vgp.tutor.jvfModel;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Vector;

import jv.loader.PgLoader;
import jv.number.PuDouble;
import jv.object.PsConfig;
import jv.object.PsDebug;
import jv.object.PsObject;
import jv.object.PsPanel;
import jv.object.PsUtil;
import jv.project.PgGeometry;
import jv.viewer.PvDisplay;
import jv.viewer.PvViewer;

import jvx.primitive.PgCurveF;
import jvx.primitive.PgGraphF;
import jvx.primitive.PgPlotF;
import jvx.surface.PgParmSurface;

/**
 * WORK IN PROGRESS. NO HDPI SUPPORT.
 * 
 * Load JVF models and provide a project inspector.
 * 
 * @see			jv.viewer.PvDisplay
 * @author		Konrad Polthier
 * @version		30.10.04, 2.10 revised (kp) Moved to package dev.primitive.<br>
 *					25.10.04, 2.00 revised (kh) Converted to JVF loader applet.<br>
 *					04.08.99, 1.00 created (kp)
 */
public class PaJvfModel extends Applet implements Runnable, ActionListener {
	/**
	 * If variable m_frame!=null then this applet runs in an application,
	 * otherwise applet runs inside an Html page within a browser.
	 * Further, when running as application then this frame is the container
	 * of the applet.
	 */
	public		Frame				m_frame			= null;
	/**
	 * 3D-viewer window for graphics output, display is embedded into the applet.
	 * This instance variable allows JavaView to perform start, stop and destroy
	 * operations when the corresponding methods of this applet are called,
	 * for example, by a browser.
	 */
	protected	PvViewer			m_viewer;
	/**
	 * Applet support. Configure and initialize the viewer,
	 * load geometry and add display.
	 */
	protected		Button		m_animButton;
	protected		PsPanel		m_panel;
	protected		PvDisplay	m_disp;
	/** Default jvf model. */
	protected		String		m_filename = "models/jvf/sofiaOrthocircles.jvf";
	// protected		String		m_filename = "dev/loader/sofiaTest.jvf";
	
	public PaJvfModel() {
		PsDebug.setNotify(true);

		m_panel		= new PsPanel();
		m_animButton	= new Button("Start Animation");
		m_animButton.addActionListener(this);
		m_panel.add(m_animButton);
	}
	
	public void init() {
		// Create a display for viewing 3d geometries. Note, more advanced
		// applets obtain display(s) from an instance of jv.viewer.PvViewer.
		
		// use the viewer:
		m_viewer	= new PvViewer(this, m_frame);
		m_disp	= (PvDisplay)m_viewer.getDisplay();
		
		String model = m_viewer.getParameter("model");
		if (model==null || model.equals(""))
			model = m_filename;
		if (model!=null && !model.equals("")) {
			if (!PsUtil.isAbsolutePath(model)) {
				model = PsConfig.getCodeBase()+model;	
			}
		}
		PgLoader loader		= new PgLoader();
		PgGeometry [] geom	= loader.loadGeometry(model);

		if( geom == null ) {
			PsDebug.warning("missing JVF model, failed loading file = "+model);	
		} else {
			for(int i=0; i<geom.length; i++) {
				m_disp.addGeometry(geom[i]);
			}
			if(geom.length>0)
				m_disp.selectGeometry(geom[0]);
		
			Vector allParams = new Vector();
			
			for(int i=0;i<geom.length;i++) {			
				Enumeration enm = null;
				if ( geom[i] instanceof PgParmSurface ) {
					PgParmSurface surf = (PgParmSurface)geom[i];
					enm = surf.getFunctionExpr().getParameters();
				} else if ( geom[i] instanceof PgCurveF) {
					PgCurveF curv = (PgCurveF)geom[i];
					enm = curv.getFunction().getParameters();
				} else if ( geom[i] instanceof PgGraphF) {
					PgGraphF gra = (PgGraphF)geom[i];
					enm = gra.getFunction().getParameters();
				} else if ( geom[i] instanceof PgPlotF) {
					PgPlotF plot = (PgPlotF)geom[i];
					enm = plot.getFunction().getParameters();
				} else {
					continue;
				}
				
				// Only arrive if global parameters were found.
				while(enm.hasMoreElements()) {
					PsObject parm = (PsObject)enm.nextElement();
						
					if (!allParams.contains(parm)) {
						m_panel.add(parm.newInspector(PsPanel.INFO_EXT));
						allParams.addElement(parm);
					}
						
					// a temporary hack:
					// animate only the first slider
					if (m_animParam == null && parm instanceof PuDouble)
						m_animParam = (PuDouble)parm;
				}
			}
		}
		
		setLayout(new BorderLayout());
		add(m_disp.getCanvas(), BorderLayout.CENTER);
		add(m_panel, BorderLayout.SOUTH);
		setSize(600, 500);
	}

	public void actionPerformed(ActionEvent event) {
		if (m_bSuspended) {
			//				animButton.
		} else {
			//				System.out.println("Not suspended!");				
		}
			
		System.out.println("Action!");
		m_bSuspended = !m_bSuspended;
			
		if (!m_bSuspended) {
			synchronized(m_thread) {
				m_thread.notify();
			}
		}
	}

	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaJvfModel app	= new PaJvfModel();
		// Create toplevel window of application containing the applet
		Frame frame	= new jv.object.PsMainFrame(app, args);
		frame.pack();
		// Store the variable frame inside the applet to indicate
		// that this applet runs as application.
		app.m_frame = frame;
		app.init();
		// In application mode, explicitly call the applet.start() method.
		app.start();
		// Set size of frame when running as application.
		frame.setSize(640, 550);
		frame.setVisible(true);
	}
	
	/**
	 * Does clean-up when applet is destroyed by the browser.
	 * Here we just close and dispose all our control windows.
	 */
	public void destroy()	{ m_viewer.destroy(); }

	///** Start viewer, e.g. start animation if requested */
	//public void start()		{ m_viewer.start(); }

	///** Stop viewer, e.g. stop animation if requested */
	//public void stop()		{ m_viewer.stop(); }
	
	/////////////////////////////////////
	//
	// Thread handling
	//
	////////////////////////////////////
	
	// Applet specific data for the thread
	protected	Thread			m_thread;
	/** Flag used to indicate that thread should stop. */
	protected	boolean			m_bRunning		= false;
	/** Flag determines if a smoothing process is currently active. */
	protected	boolean			m_bStopped		= true;
	protected	PuDouble			m_animParam		= null;
	protected	int				m_direction		= 1;
	protected	boolean			m_bSuspended	= true;

	/**
	 * Check whether smoothing process will be stopped.
	 * @return		false, if the energy minimizer will stop (just shutting down regularly).
	 */
	public boolean isRunning() { return m_bRunning; }
	/**
	 * Check whether smoothing process is currently NOT running a thread.
	 */
	public boolean isStopped() {return m_bStopped;}
	
	public void start() {
		if (!m_bStopped || m_bRunning)
			return;
		
		m_bRunning = true;
		m_thread = new Thread(this, "JavaView: Animation");
		m_thread.setPriority(Thread.NORM_PRIORITY);
		
		m_thread.start();
	}
	
	public void stop() {
		if (!m_bRunning)
			return;
		
		m_bRunning	= false;
		m_thread		= null;

		//			if (m_infoPanel!=null)
		//				m_infoPanel.update(this);
		m_panel.update(this);
	}

	public void run() {
		if (m_animParam == null)
			return;
		
		m_bStopped = false;
		while(m_thread != null && m_bRunning) {
			if (m_bSuspended) {
				synchronized(m_thread) {
					try {
						while (m_bSuspended)
							m_thread.wait();
					} catch (InterruptedException e) {}
				}
			}

			/* Do the main loop here */
			double newValue = m_animParam.getValue()+m_direction*m_animParam.getLineIncr();
			if (newValue <= m_animParam.getMin() || newValue >= m_animParam.getMax()) {
				m_direction = -m_direction;
				newValue = m_animParam.getValue()+m_direction*m_animParam.getLineIncr();
			}
			// PsDebug.message("iteration = "+String.valueOf(cnt++));

			m_disp.setEnabledRepaint(false);
			
			// move the slider
			m_animParam.setValue(newValue);
			m_animParam.update(null);
			
			m_disp.setEnabledRepaint(true);
			m_disp.repaint();
			
			try {
				Thread.sleep(40);
			} catch (Exception e) {}
			
		}
		m_bStopped = true;
		// update(this);
	}
}
