package vgp.tutor.texture;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.number.PuColorPicker;
import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.objectGui.PsImage;
import jv.project.PjProject_IP;

/**
 * Control panel for loading and resetting surface texture.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 1.30 revised (kp) Doc and notice added.<br>
 *					14.09.06, 1.20 revised (kp) Now use checkbox to switch background image.<br>
 *					15.08.99, 1.10 revised (kp) Converted to AWT1.1 event model.<br>
 *					02.01.99, 1.00 created (kp) 
 */
public class PjTexturedSurface_IP extends PjProject_IP implements ActionListener, ItemListener { 
	protected	PjTexturedSurface		m_pjTexture;
	protected	Checkbox					m_cLoadBackImage;
	protected	Button					m_bReset;

	/** Constructor */
	public PjTexturedSurface_IP() {
		super();
		if (getClass() == PjTexturedSurface_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		// buttons at bottom
		Panel m_pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		add(m_pBottomButtons);
		m_cLoadBackImage = new Checkbox("Show Background Image");
		m_cLoadBackImage.addItemListener(this);
		m_pBottomButtons.add(m_cLoadBackImage);
		m_bReset = new Button("Reset");
		m_bReset.addActionListener(this);
		m_pBottomButtons.add(m_bReset);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Textured surface with optional background image in display.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjTexture = (PjTexturedSurface)parent;
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("PjTexturedSurface_IP.update: isShowing = "+isShowing());
		if (m_pjTexture == event) {
			PsPanel.setState(m_cLoadBackImage, (m_pjTexture.m_image != null));
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle item events.
	 */
	public void itemStateChanged(ItemEvent event) {
		if (m_pjTexture==null)
			return;
		Object source = event.getSource();
		if (source == m_cLoadBackImage) {
			if (m_pjTexture.m_image == null) {
				m_pjTexture.m_image = new PsImage(PuColorPicker.getWheel(200, 200, 255, null), "Sample Image");
				// Image loading is done in later call of m_image#getImage().
			} else {
				m_pjTexture.m_image = null;
			}
			m_pjTexture.update(m_pjTexture);
		}
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjTexture==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjTexture.m_image = null;
			m_pjTexture.init();
			m_pjTexture.update(m_pjTexture);
		}
	}
}

