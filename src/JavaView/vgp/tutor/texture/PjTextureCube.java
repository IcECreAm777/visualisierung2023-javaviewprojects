package vgp.tutor.texture;

import java.awt.Image;

import jv.geom.PgElementSet;
import jv.geom.PgTexture;
import jv.object.PsDebug;
import jv.objectGui.PsImage;
import jv.project.PjProject;
import jv.project.PvDisplayIf;
import jv.vecmath.PdVector;

/**
 * Demo project for using texture on surfaces showing a cube with image on each side.
 * Each face of the cube is a separate geometry.
 * 
 * @see			jv.geom.PgPointSet
 * @see			jv.geom.PgElementSet
 * @see			jv.geom.PgTexture
 * @author		Konrad Polthier
 * @version		03.08.99, 1.00 revised (kp) <br>
 *					03.08.99, 1.00 created (kp)
 */
public class PjTextureCube extends PjProject implements Runnable {
	protected	static final String []	m_texImage = {
		"vgp/tutor/texture/images/T_Rex.jpg",
		"vgp/tutor/texture/images/CubeFlow.jpg",
		"vgp/tutor/texture/images/Auge.jpg",
		"vgp/tutor/texture/images/Leuchte.jpg",
		"vgp/tutor/texture/images/Himmel.jpg",
		"vgp/tutor/texture/images/City2.jpg"
	};
	protected	PgElementSet []			m_faceArr;
	public PjTextureCube() {
		super("Textured Cube Demo");
		m_faceArr	= new PgElementSet[6];
		for (int i=0; i<6; i++) {
			m_faceArr[i] = new PgElementSet(3);
		}
		if (getClass() == PjTextureCube.class) {
			init();
		}
	}
	public void init() {
		super.init();

		PdVector [] quadr = new PdVector[4];
		quadr[0] = new PdVector(0., 0., 1.);
		quadr[1] = new PdVector(1., 0., 1.);
		quadr[2] = new PdVector(1., 1., 1.);
		quadr[3] = new PdVector(0., 1., 1.);
		m_faceArr[0].buildQuadr(quadr, 2, 2);
		m_faceArr[0].setName("Top");

		quadr = new PdVector[4];
		quadr[0] = new PdVector(0., 0., 0.);
		quadr[1] = new PdVector(1., 0., 0.);
		quadr[2] = new PdVector(1., 0., 1.);
		quadr[3] = new PdVector(0., 0., 1.);
		m_faceArr[1].buildQuadr(quadr, 2, 2);
		m_faceArr[1].setName("Front");

		quadr = new PdVector[4];
		quadr[0] = new PdVector(0., 1., 0.);
		quadr[1] = new PdVector(0., 0., 0.);
		quadr[2] = new PdVector(0., 0., 1.);
		quadr[3] = new PdVector(0., 1., 1.);
		m_faceArr[2].buildQuadr(quadr, 2, 2);
		m_faceArr[2].setName("Left");

		quadr = new PdVector[4];
		quadr[0] = new PdVector(1., 0., 0.);
		quadr[1] = new PdVector(1., 1., 0.);
		quadr[2] = new PdVector(1., 1., 1.);
		quadr[3] = new PdVector(1., 0., 1.);
		m_faceArr[3].buildQuadr(quadr, 2, 2);
		m_faceArr[3].setName("Right");

		quadr = new PdVector[4];
		quadr[0] = new PdVector(1., 1., 0.);
		quadr[1] = new PdVector(0., 1., 0.);
		quadr[2] = new PdVector(0., 1., 1.);
		quadr[3] = new PdVector(1., 1., 1.);
		m_faceArr[4].buildQuadr(quadr, 2, 2);
		m_faceArr[4].setName("Back");

		quadr = new PdVector[4];
		quadr[0] = new PdVector(1., 0., 0.);
		quadr[1] = new PdVector(0., 0., 0.);
		quadr[2] = new PdVector(0., 1., 0.);
		quadr[3] = new PdVector(1., 1., 0.);
		m_faceArr[5].buildQuadr(quadr, 2, 2);
		m_faceArr[5].setName("Bottom");

		for (int i=0; i<6; i++) {
			m_faceArr[i].makeVertexNormals();
			m_faceArr[i].makeElementNormals();
			// m_face[i].showBackface(false);
			addGeometry(m_faceArr[i]);
		}
		selectGeometry(m_faceArr[0]);
	}
	public void start() {
		if (PsDebug.NOTIFY) PsDebug.notify("PjTextureCube.start: ");
		PvDisplayIf disp = getDisplay();
		if (disp != null) {
			disp.setEnabledAnimation(true);
			disp.setAutoRotation(new PdVector(0., .4, .6), .3);
		}
		Thread thread = new Thread(this, "JavaView: loading texture images");
		thread.start();
		super.start();
	}
	public void run() {
		for (int i=0; i<6; i++) {
			Image img = PsImage.getImageResource(m_texImage[i]);
			if (img == null)
				continue;

			m_faceArr[i].makeQuadrElementTexture(2, 2);
			PgTexture texture = new PgTexture();
			texture.setImage(img);
			texture.setImageName(m_texImage[i]);
			m_faceArr[i].setTexture(texture);
			m_faceArr[i].showElementTexture(true);
			m_faceArr[i].update(m_faceArr[i]);
		}
	}
}

