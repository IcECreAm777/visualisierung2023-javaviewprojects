package vgp.tutor.texture;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Control panel for loading and resetting surface texture.
 * 
 * @author		Konrad Polthier
 * @version		29.10.16, 1.00 created (kp) Based on PjTexturedSurface_IP.<br>
 */
public class PjTextureCube_IP extends PjProject_IP { 
	protected	PjTextureCube			m_pjTexture;

	/** Constructor */
	public PjTextureCube_IP() {
		super();
		if (getClass() == PjTextureCube_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Cube is formed by six independent face geometries, each face has an individual texture image. "+
							 "The display is in auto-rotation mode. Press key-q or key-w to stop or start "+
							 "auto-rotation mode in a display.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjTexture = (PjTextureCube)parent;
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("PjTexturedCube_IP.update: isShowing = "+isShowing());
		if (m_pjTexture == event) {
			return true;
		}
		return super.update(event);
	}
}

