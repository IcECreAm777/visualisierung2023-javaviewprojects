package vgp.tutor.lsystem;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Generate a tree with an L-system and display using turtle graphics.
 * Additionally, this subclass implements the interface PsUpdateIf
 * to catch events from a parameter slider.
 * 
 * @see			vgp.tutor.lsystem.PjLSystem
 * @author		Konrad Polthier
 * @version		08.11.16, 3.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				30.10.01, 2.00 revised (kp) L-System converted into a project.<br>
 *					17.02.00, 1.10 revised (kp) Slider added.<br>
 *					16.02.00, 1.00 created (kp)
 */
public class PaLSystem extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "3.00" + "\r\n" +
				 "Demo applet computes l-systems" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjLSystem();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaLSystem(), args);
	}
}
