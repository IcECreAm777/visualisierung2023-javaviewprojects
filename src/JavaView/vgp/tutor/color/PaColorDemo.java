package vgp.tutor.color;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;

import jv.number.PdColor;
import jv.number.PdColor_Dialog;
import jv.number.PuColorPicker;
import jv.object.PsConfig;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;

/**
 * Applet shows color picker inside an applet or, if run standalone, in a separate dialog.
 * The applet registers itself as update listener of the color picker and
 * is informed whenever the color changes. As a demonstration the applet
 * prints the RGB components of the current color in a label at the bottom.
 * <p>
 * The application does not register as update listener and is
 * therefore not informed when the color changes.
 * 
 * @version		19.08.03, 2.00 revised (kp) Applet registers as listener and print the color.<br>
 */
public class PaColorDemo extends Applet implements PsUpdateIf {
	/** Color picker shown in applet mode. */
	protected		PuColorPicker		myDude;
	/** Label shows the received color as string. */
	protected		static Label		label;
	// Show color picker inside the applet
	public void init() {
		// JavaView's configuration class needs to know if running as applet or application.
		PsConfig.init(this, null);

		myDude = new PuColorPicker(Color.orange);
		myDude.setTitle("Color Selector");
		myDude.setBorderType(PsPanel.BORDER_GROOVE);
		
		myDude.setParent(this);
		
		setLayout(new BorderLayout());
		add(myDude, BorderLayout.CENTER);
		
		label = new Label("Pick a color!", Label.CENTER);
		add(label, BorderLayout.SOUTH);
	}
	// Show color picker in separate dialog
	public static void main(String[] args) {
		// JavaView's configuration class needs to know if running as applet or application.
		PsConfig.init(null, null);

		PdColor col = new PdColor("myColor", null);
		col.setColor(Color.blue);
		PdColor_Dialog dialog = new PdColor_Dialog(col);
		dialog.setTitle("Color Dialog");
		dialog.setVisible(true);
	}
	/** Get name of instance.*/
	public		String		getName() { return "Color Selector"; }
	/** Get parent and do nothing else. */
	public		PsUpdateIf	getFather() { return null; }
	/** Set parent and do nothing else. */
	public		void			setParent(PsUpdateIf parent) { return; }
	/**
	 * Event handling method in the update mechanism.
	 * 
	 * @param		event			carries a lot of information
	 * @return		true			if event has been handled, otherwise false
	 */
	public		boolean		update(Object event) {
		Color c = null;
		if (event == myDude) {
			c = myDude.getColor();
		} else
			return false;
		label.setText("Picked Color = ("+c.getRed()+", "+c.getGreen()+", "+c.getBlue()+")");
		return true;
	}
}
