package vgp.tutor.gui;

import java.applet.Applet;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsConfig;
import jv.object.PsMainFrame;
import jv.object.PsStackLayout;

import jvx.gui.PuProgressBar;

/**
 * Applet shows usage of progress bar (without time information).
 *
 * @see			jvx.gui.PuProgressBar
 * @author		Konrad Polthier
 * @version		20.01.07, 1.00 revised (kp) <br>
 *					20.01.07, 1.00 created (kp)
 */
public class PaProgressBar extends Applet implements ActionListener, Runnable {
	/** Progress bar. */
	protected	PuProgressBar		m_progressBar;
	/** Button starts a calculation. */
	protected	Button				m_bStart;

	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of progress bar" + "\r\n";
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void init() {
		m_progressBar	= new PuProgressBar();
		m_progressBar.setText("Demo Bar");

		setLayout(new PsStackLayout(2));

		Label title	= new Label("Demo of Progress Bar");
		title.setFont(PsConfig.getFont(PsConfig.FONT_HEADER4));
		add(title);

		Panel pButton	= new Panel(new FlowLayout());
		{
			m_bStart		= new Button("Start");
			m_bStart.addActionListener(this);
			pButton.add(m_bStart);
		}
		add(pButton);

		add(m_progressBar.getInfoPanel());
	}

	public void actionPerformed(ActionEvent event) {
		Object source		= event.getSource();
		if (source == m_bStart) {
			m_progressBar.start();
			(new Thread(this, "Progress Bar Applet")).start();
			m_bStart.setEnabled(false);
		}
	}

	/**
	 * When an object implementing interface <code>Runnable</code> is used
	 * to create a thread, starting the thread causes the object's
	 * <code>run</code> method to be called in that separately executing
	 * thread.
	 * <p>
	 * The general contract of the method <code>run</code> is that it may
	 * take any action whatsoever.
	 *
	 * @see Thread#run()
	 */
	public void run() {
		// Value determines progress of current calculation, range is in [0.,1.].
		double progressValue	= 0.;
		m_progressBar.setProgress(progressValue);

		// Loop of calculation.
		while (progressValue < 1.) {
			// Instead of sleeping, do some calculation.
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}

			progressValue	+= 0.01;
			m_progressBar.setProgress(progressValue);
		}
		m_bStart.setEnabled(true);
		m_progressBar.stop();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaProgressBar va	= new PaProgressBar();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(va, args);
		va.init();
		frame.setOuterBounds(420, 5, 300, 300);
		frame.setVisible(true);
	}
}
