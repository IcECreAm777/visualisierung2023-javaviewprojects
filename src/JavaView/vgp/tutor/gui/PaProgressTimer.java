package vgp.tutor.gui;

import java.applet.Applet;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsConfig;
import jv.object.PsMainFrame;
import jv.object.PsStackLayout;

import jvx.gui.PuProgressBar;

/**
 * Applet shows usage of progress bar (with time information).
 * 
 * @see			jvx.gui.PuProgressBar
 * @author		Konrad Polthier
 * @version		20.01.07, 1.00 revised (kp) <br>
 *					20.01.07, 1.00 created (kp) 
 */
public class PaProgressTimer extends Applet implements ActionListener, Runnable {
	/** Progress bar. */
	protected	PuProgressBar		m_progressBar;
	/** Value determines progress of current calculation, range is in [0.,1.]. */
	protected	double				m_progressValue;
	/** Button starts a calculation. */
	protected	Button				m_bStart;
	/** Button stops a calculation. */
	protected	Button				m_bAbort;
	/** Flag if calculation is running. */
	protected	boolean				m_bRunning;
	/** Thread which performs the calculation. */
	protected	Thread				m_thread;
	/** Flag if timer thread is in wait status. */
	protected	boolean				m_bSuspended;

	
	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of progress bar" + "\r\n";
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void init() {
		m_progressBar		= new PuProgressBar(true);
		m_progressBar.setText("Demo Timer");
		m_progressValue	= 0.;

		setLayout(new PsStackLayout(2));

		Label title	= new Label("Demo of Progress Timer");
		title.setFont(PsConfig.getFont(PsConfig.FONT_HEADER4));
		add(title);
		
		Panel pButton	= new Panel(new FlowLayout());
		{
			m_bStart		= new Button("Start");
			m_bStart.addActionListener(this);
			pButton.add(m_bStart);
			
			m_bAbort		= new Button("Abort");
			m_bAbort.addActionListener(this);
			m_bAbort.setEnabled(false);
			pButton.add(m_bAbort);
		}
		add(pButton);

		add(m_progressBar.getInfoPanel());
	}
	
	public		void actionPerformed(ActionEvent event) {
		Object source		= event.getSource();
		if (source == m_bStart) {
			String currLabel	= m_bStart.getLabel();
			if (currLabel.equals("Start")) {
				m_progressBar.reset();
				m_progressValue	= 0.;
				m_bRunning			= true;
				m_bSuspended		= false;
				m_thread				= new Thread(this, "JavaView Applet");
				m_thread.start();
				m_bStart.setLabel("Pause");
			} else if (currLabel.equals("Cont.")) {
				m_bRunning			= true;
				m_progressBar.resumeTimer();
				m_bSuspended		= false;
				synchronized (m_thread) {
					m_thread.notify();
				}
				m_bStart.setLabel("Pause");
			} else if (currLabel.equals("Pause")) {
				m_bRunning			= false;
				m_progressBar.suspendTimer();
				m_bSuspended		= true;
				m_bStart.setLabel("Cont.");
			}
			m_bAbort.setEnabled(true);
		} else if (source == m_bAbort) {
			m_bRunning			= false;
			m_bSuspended		= false;
			m_progressBar.abortTimer();
			m_bStart.setLabel("Start");
			m_bAbort.setEnabled(false);
		} else {
			return;
		}
	}

	/**
	 * When an object implementing interface <code>Runnable</code> is used
	 * to create a thread, starting the thread causes the object's
	 * <code>run</code> method to be called in that separately executing
	 * thread.
	 * <p>
	 * The general contract of the method <code>run</code> is that it may
	 * take any action whatsoever.
	 *
	 * @see Thread#run()
	 */
	public void run() {
		// Loop of calculation.
		while (m_bRunning && m_progressValue<1.) {
			m_progressValue	+= 0.01;
			m_progressBar.setProgress(m_progressValue);

			// Instead of sleeping, do some calculation.
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}
			// In suspended mode this thread performs the standard wait() iteration.
			if (m_bSuspended) {
				synchronized (m_thread) {
					while (m_bSuspended) {
						try {
							wait(1000);
						} catch (Exception ex) {}
					}
				}
			}
		}
		m_bRunning	= false;
		m_bStart.setLabel("Start");
		m_bAbort.setEnabled(false);
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaProgressTimer va	= new PaProgressTimer();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(va, args);
		va.init();
		frame.setOuterBounds(420, 5, 300, 300);
		frame.setVisible(true);
	}
}
