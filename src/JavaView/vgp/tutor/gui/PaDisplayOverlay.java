package vgp.tutor.gui;

import java.awt.Rectangle;

import jv.geom.PgElementSet;
import jv.object.PsObject;
import jv.object.PsUpdateIf;
import jv.project.PjProject;
import jv.project.PvApplet;
import jv.vecmath.PdVector;
import jv.vecmath.PiVector;
import jv.viewer.PvDisplay;

import jvx.gui.PuColorBar;
import jvx.gui.PuColorBarCanvas;

/**
 * Applet shows usage of external overlay canvas inside a JavaView display.
 *
 * @see			jvx.gui.PuColorBarCanvas
 * @author		Konrad Polthier
 * @version		07.05.17, 2.00 revised (kp) New superclass jv.project.PvApplet.<br>
 * 				10.03.15, 1.00 created (mn)
 */
public class PaDisplayOverlay extends PvApplet implements PsUpdateIf {
	/** Color bar. */
	protected PuColorBar			m_colorBar;
	/** Color bar canvas. */
	protected PuColorBarCanvas	m_colorBarCanvas;
	/** Geometry. */
	protected PgElementSet		m_geom;
	/** Value for each element which codes the color. */
	protected PdVector			m_elementValues;

	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of external overlay canvas inside a JavaView display" + "\r\n";
	}
	/** Return a new allocated project instance. */
	public Rectangle getSizeOfFrame() {
		return new Rectangle(375, 5, 640, 512);
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return null;
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void run() {
		// Compute a sphere
		m_geom = new PgElementSet(3);
		m_geom.computeSphere(20, 20, 1.);
		m_geom.makeElementNormals();
		m_geom.showEdges(false);
		m_geom.showElementColors(true);
		
		// Add color bar
		m_colorBar = new PuColorBar(true, true, true);
		m_colorBar.setParent(this);
		initColorBar(m_geom);
		// Colorize geometry
		colorizeGeometry(m_geom);

		PvDisplay display = (PvDisplay)super.getDisplay();
		display.addOverlay(m_colorBar.getCanvas());
		display.addGeometry(m_geom);

		super.run();
	}

	/** Creates a demo geometry and set its properties. */
	private void initColorBar(PgElementSet geom) {
		// Compute a value for each element and set minValue and maxValue in the color bar.
		int numElements = geom.getNumElements();
		m_elementValues = new PdVector(numElements);
		double minValue = Double.POSITIVE_INFINITY;
		double maxValue = Double.NEGATIVE_INFINITY;
		for (int e=0; e<numElements; e++) {
			PiVector element = geom.getElement(e);
			int dim = element.getSize();
			m_elementValues.m_data[e] = 0.;
			for (int i=0; i<dim; i++)
				m_elementValues.m_data[e] += geom.getVertex(element.m_data[i]).m_data[2];
			m_elementValues.m_data[e] /= dim;
			if (m_elementValues.m_data[e] > maxValue)
				maxValue = m_elementValues.m_data[e];
			if (m_elementValues.m_data[e] < minValue)
				minValue = m_elementValues.m_data[e];
		}
		m_colorBar.setMinValue(minValue);
		m_colorBar.setMaxValue(maxValue);
	}

	/** Make element colors of the geometry according to the element values. */
	private void colorizeGeometry(PgElementSet geom) {
		if ((geom == null) || (m_elementValues == null))
			return;
		int numE = geom.getNumElements();
		for (int e=0; e<numE; e++) {
			geom.setElementColor(e, m_colorBar.getColor(m_elementValues.m_data[e]));
			if (m_colorBar.isMarked(m_elementValues.m_data[e]))
				geom.setTagElement(e, PsObject.IS_SELECTED);
			else
				geom.clearTagElement(e, PsObject.IS_SELECTED);
		}
	}

	/**
	 * Get parent and do nothing else.
	 * <p>
	 * Historical note: previous to Java 1.1 this method was called getParent()
	 * in JavaView, but in Java 1.1 a method with same name was introduced in
	 * java.awt.Component which prevented us to continue to this method
	 * in any of our subclasses of AWT components. In order to be consistent,
	 * the JavaView method getParent() was replaced with getFather() everywhere.
	 * Since that time JavaView uses the pair setParent/getFather,
	 * and lives quite well with that.
	 *
	 * @see			#setParent(jv.object.PsUpdateIf)
	 * @see			#update(Object)
	 */
	public PsUpdateIf getFather() {
		return null;
	}

	/**
	 * Set parent and do nothing else.
	 * Method is implemented by PsObject and PsPanel, and should be called if overwritten.
	 *
	 * @param		parent		will receive events which <code>this</code> does not handle
	 * @see			#getFather()
	 * @see			#update(Object)
	 */
	public void setParent(PsUpdateIf parent) {
	}

	/**
	 * Event handling method in the update mechanism.
	 * Events not handled will be passed to m_parent or super in this
	 * sequence.
	 *
	 * @param		event			carries a lot of information
	 * @return 		true			if event has been handled, otherwise false
	 * @see			jv.object.PsObject
	 * @see			#getFather()
	 * @see			#setParent(jv.object.PsUpdateIf)
	 */
	public boolean update(Object event) {
		if (event == m_colorBar) { // state of the color bar has been changed
			colorizeGeometry(m_geom);
			m_geom.update(m_geom);
			return true;
		}
		return false;
	}

	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaDisplayOverlay(), args);
	}
}
