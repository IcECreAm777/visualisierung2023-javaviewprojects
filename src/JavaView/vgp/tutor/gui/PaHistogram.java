package vgp.tutor.gui;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsConfig;
import jv.object.PsMainFrame;
import jv.vecmath.PdVector;

import jvx.gui.PuHistogram;


/**
 * Applet shows usage of histogram.
 * 
 * @see			jvx.gui.PuHistogram
 * @author		Konrad Polthier
 * @version		27.05.07, 1.10 revised (kp) Adjusted to modified histogram.<br>
 *					20.05.07, 1.00 created (kp) 
 */
public class PaHistogram extends Applet implements ActionListener {
	/** Parent frame. */
	protected	Frame					m_frame;
	/** Progress bar. */
	protected	PuHistogram			m_histogram;
	/** Button generates a new random sample set. */
	protected	Button				m_bStart;
	/** Vector containing the samples. */
	protected	PdVector				m_samples;
	
	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of histogram" + "\r\n";
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void init() {
		PsConfig.init(this, m_frame);
	
		setLayout(new BorderLayout());

		Label title	= new Label("Demo of Histogram");
		title.setFont(PsConfig.getFont(PsConfig.FONT_HEADER4));
		add(title, BorderLayout.NORTH);
		
		m_histogram	= new PuHistogram("Tutorial Histogram", null);
		m_histogram.setClassType(PuHistogram.NUMBINS_USER);
		m_histogram.setCountType(PuHistogram.COUNT_ABSOLUTE);
		m_histogram.setEnabledSampleBnd(true);
		m_histogram.setSampleBnd(-4., 4.);
		m_histogram.setNumBins(20);
		this.computeSamples();
		m_histogram.update(m_histogram);
		add(m_histogram.getInfoPanel(), BorderLayout.CENTER);
		
		Panel pBottom	= new Panel(new FlowLayout());
		{
			m_bStart		= new Button("New Random Set");
			m_bStart.addActionListener(this);
			pBottom.add(m_bStart);
		}
		add(pBottom, BorderLayout.SOUTH);
	}

	public void actionPerformed(ActionEvent event) {
		Object source		= event.getSource();
		if (source == m_bStart) {
			this.computeSamples();
			m_histogram.update(m_histogram);
		}
	}

	/** Compute randomly generated samples. */
	private void computeSamples() {
		int numSamples		= 5000;
		m_samples			= new PdVector(numSamples);
		for (int i=0; i<numSamples; i++) {
			double val;
			do {
				double u1 	= Math.random();
				double u2 	= Math.random();
				val			= Math.sqrt(-2*Math.log(u1))*Math.cos(2.*Math.PI*u2);
			} while (Math.abs(val) > 10.);
			m_samples.setEntry(i, val);
		}
		m_histogram.setSamples(m_samples);
		m_histogram.setTitle("Demo Histogram");
		m_histogram.setXAxisLabel("Random Numbers");
	}
	
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaHistogram va		= new PaHistogram();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(va, args);
		frame.setOuterBounds(420, 5, 600, 450);
		va.m_frame = frame;
		va.init();
		va.m_frame.setVisible(true);
	}
}
