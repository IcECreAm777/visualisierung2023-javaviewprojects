package vgp.tutor.gui;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;

import jv.geom.PgElementSet;
import jv.object.PsConfig;
import jv.object.PsMainFrame;
import jv.object.PsObject;
import jv.object.PsStackLayout;
import jv.object.PsUpdateIf;
import jv.project.PvDisplayIf;
import jv.vecmath.PdVector;
import jv.vecmath.PiVector;
import jv.viewer.PvViewer;

import jvx.gui.PuColorBar;

/**
 * Applet shows usage of color bar.
 *
 * @see			jvx.gui.PuColorBar
 * @author		Matthias Nieser
 * @version		28.03.07, 1.00 created (mn)
 */
public class PaColorBar extends Applet implements PsUpdateIf {
	/** Main frame. */
	protected Frame m_frame;
	/** Geometry. */
	protected PgElementSet m_geom;
	/** Color bar. */
	protected PuColorBar m_colorBar;
	/** Value for each element which codes the color. */
	protected PdVector m_elementValues;
	/** Button starts a calculation. */
	protected Button m_bStart;

	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Matthias Nieser" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of color bar" + "\r\n";
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void init() {
		// Make layout
		removeAll();

		// Add color bar
		m_colorBar = new PuColorBar(true, true, true);
		Panel p = new Panel(new PsStackLayout(2));
		Label title	= new Label("Demo of Color Bar");
		title.setFont(PsConfig.getFont(PsConfig.FONT_HEADER4));
		p.add(title);
		p.add(m_colorBar.getInfoPanel());
		setLayout(new BorderLayout());
		add(p, BorderLayout.NORTH);
		m_colorBar.setParent(this);

		// Add JavaView display
		PvViewer viewer = new PvViewer(this, m_frame);
		PvDisplayIf display = viewer.getDisplay();
		if (m_geom == null) {
			initGeom();
			display.addGeometry(m_geom);
		}
		add((Component)(display), BorderLayout.CENTER);
	}

	/** Creates a demo geometry and set its properties. */
	private void initGeom() {
		// Compute a sphere
		m_geom = new PgElementSet(3);
		m_geom.computeSphere(20, 20, 1.);
		m_geom.makeElementNormals();
		m_geom.showEdges(false);
		m_geom.showElementColors(true);

		// Compute a value for each element and set minValue and maxValue in the color bar.
		int numElements = m_geom.getNumElements();
		m_elementValues = new PdVector(numElements);
		double minValue = Double.POSITIVE_INFINITY;
		double maxValue = Double.NEGATIVE_INFINITY;
		for (int e=0; e<numElements; e++) {
			PiVector element = m_geom.getElement(e);
			int dim = element.getSize();
			m_elementValues.m_data[e] = 0.;
			for (int i=0; i<dim; i++)
				m_elementValues.m_data[e] += m_geom.getVertex(element.m_data[i]).m_data[2];
			m_elementValues.m_data[e] /= dim;
			if (m_elementValues.m_data[e] > maxValue)
				maxValue = m_elementValues.m_data[e];
			if (m_elementValues.m_data[e] < minValue)
				minValue = m_elementValues.m_data[e];
		}
		m_colorBar.setMinValue(minValue);
		m_colorBar.setMaxValue(maxValue);

		// colorize geometry
		colorizeGeometry();
	}

	/** Make element colors of the geometry according to the element values. */
	private void colorizeGeometry() {
		if ((m_geom == null) || (m_elementValues == null))
			return;
		int numE = m_geom.getNumElements();
		for (int e=0; e<numE; e++) {
			m_geom.setElementColor(e, m_colorBar.getColor(m_elementValues.m_data[e]));
			if (m_colorBar.isMarked(m_elementValues.m_data[e]))
				m_geom.setTagElement(e, PsObject.IS_SELECTED);
			else
				m_geom.clearTagElement(e, PsObject.IS_SELECTED);
		}
	}

	/**
	 * Get parent and do nothing else.
	 * <p>
	 * Historical note: previous to Java 1.1 this method was called getParent()
	 * in JavaView, but in Java 1.1 a method with same name was introduced in
	 * java.awt.Component which prevented us to continue to this method
	 * in any of our subclasses of AWT components. In order to be consistent,
	 * the JavaView method getParent() was replaced with getFather() everywhere.
	 * Since that time JavaView uses the pair setParent/getFather,
	 * and lives quite well with that.
	 *
	 * @see			#setParent(jv.object.PsUpdateIf)
	 * @see			#update(Object)
	 */
	public PsUpdateIf getFather() {
		return null;
	}

	/**
	 * Set parent and do nothing else.
	 * Method is implemented by PsObject and PsPanel, and should be called if overwritten.
	 *
	 * @param		parent		will receive events which <code>this</code> does not handle
	 * @see			#getFather()
	 * @see			#update(Object)
	 */
	public void setParent(PsUpdateIf parent) {
	}

	/**
	 * Event handling method in the update mechanism.
	 * Events not handled will be passed to m_parent or super in this
	 * sequence.
	 *
	 * @param		event			carries a lot of information
	 * @return true			if event has been handled, otherwise false
	 * @see			jv.object.PsObject
	 * @see			#getFather()
	 * @see			#setParent(jv.object.PsUpdateIf)
	 */
	public boolean update(Object event) {
		if (event == m_colorBar) { // state of the color bar has been changed
			colorizeGeometry();
			m_geom.update(m_geom);
			return true;
		}

		return false;
	}

	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaColorBar va	= new PaColorBar();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(va, args);
		frame.setOuterBounds(420, 5, 300, 300);
		va.m_frame = frame;
		va.init();
		va.m_frame.setVisible(true);
	}
}
