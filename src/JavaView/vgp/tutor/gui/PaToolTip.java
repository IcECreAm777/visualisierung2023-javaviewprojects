package vgp.tutor.gui;

import java.applet.Applet;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsConfig;
import jv.object.PsMainFrame;

import jvx.gui.PsToolTip;

/**
 * Applet shows usage of tooltip. Text of tooltip can be dynamically adjusted.
 *
 * @see			jvx.gui.PsToolTip
 * @author		Konrad Polthier
 * @version		21.03.10, 1.00 revised (kp) <br>
 *					21.03.10, 1.00 created (kp)
 */
public class PaToolTip extends Applet implements ActionListener {
	/** Button starts a calculation. */
	protected	Button					m_bButton;
	/** Memorize the tooltip instance in order to change displayed text. */
	protected	PsToolTip				m_tTooltip;

	/** Interface of applet to inform about author, version, and copyright */
	public		String				getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "1.00" + "\r\n" +
				 "Applet shows usage of tooltip" + "\r\n";
	}

	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void init() {
		// setLayout(new PsStackLayout(2));
		setLayout(new GridLayout(2, 1));

		Label title	= new Label("Demo of Tooltip");
		title.setFont(PsConfig.getFont(PsConfig.FONT_HEADER4));
		add(title);

		m_bButton		= new Button("Need Info?");
		m_bButton.addActionListener(this);
		m_tTooltip = new PsToolTip(m_bButton, "Pressing the button toogles display of tooltip");
		add(m_bButton);
	}

	public void actionPerformed(ActionEvent event) {
		Object source		= event.getSource();
		if (source == m_bButton) {
			m_bButton.setLabel("No more info needed ...");
			m_tTooltip.setTipText("You've lost, no more info displayed");
		}
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PaToolTip va		= new PaToolTip();
		// Create toplevel window of application containing the applet
		PsMainFrame frame	= new PsMainFrame(va, args);
		va.init();
		frame.setOuterBounds(420, 5, 300, 500);
		frame.setVisible(true);
	}
}
