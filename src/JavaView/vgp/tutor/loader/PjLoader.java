package vgp.tutor.loader;

import java.io.BufferedReader;
import java.io.IOException;

import jv.geom.PgElementSet;
import jv.geom.PgPointSet;
import jv.geom.PgPolygon;
import jv.geom.PgPolygonSet;
import jv.object.PsConfig;
import jv.object.PsDebug;
import jv.object.PsUtil;
import jv.project.PgGeometryIf;
import jv.project.PgJvxSrc;
import jv.project.PjProject;
import jv.project.PvGeometryIf;
import jv.project.PvViewerIf;

/**
 * Tutorial on geometry loaders explains how to write
 * and use an own geometry loader within JavaView.
 * 
 * @see			vgp.tutor.loader.PgAbcLoader
 * @author		Konrad Polthier
 * @version		15.12.16, 1.00 created (kp) Based on run() code of PaLoader.<br>
 */
public class PjLoader extends PjProject {
	/** Applet parameters: {"Name", "Type", "Default value", "Description"} */
	public		static final String [][]		m_parmLoader = {
		{"filename",		"String",	"vgp/tutor/loader/cube.abc",	"Default geometry file"},
		{"vgp.tutor.loader.indexfirstvertex",	"String",	"0",		"Index of first vertex"}
	};

	public PjLoader() {
		super("File Loader Demo");
		
		// Register applet/project parameters in superclass.
		setParameterInfo(m_parmLoader);

		if (getClass() == PjLoader.class) {
			init();
		}
	}
	public void init() {
		super.init();
	}
	public void start() {
		if (PsDebug.NOTIFY) PsDebug.notify("called");

		PvViewerIf viewer = getViewer();
		if (viewer != null) {
			// Get the name of the geometry file either as applet parameter
			// or, if running as application, as command line argument.
			String fileName		= viewer.getParameter("filename");
			String ext				= PsUtil.getFileExtension(fileName);
			if (ext == null) {
				if (PsDebug.WARNING) PsDebug.warning("missing extension in file="+fileName);
			}

			// The geometries will be generate in the parser. This parser
			// returns exactly one geometry.
			PgJvxSrc [] geomArr	= null;
			
			// Open the file resp. URL. Getting the codeBase from PsConfig
			// avoids a distinction between applets and applications.
			BufferedReader in = PsUtil.open(PsConfig.getCodeBase()+fileName);
			if (in == null) {
				if (PsDebug.WARNING) PsDebug.warning("could not open file = "+fileName);
			} else {
				// Parse the file
				PgAbcLoader loader	= new PgAbcLoader();
				String index			= viewer.getParameter("vgp.tutor.loader.indexfirstvertex");
				if (index != null) {
					int ind = Integer.parseInt(index);
					loader.setFirstVertexIndex(ind);
				}
				geomArr = loader.read(in);

				// Before we continue, first close the file.
				try { in.close(); } catch (IOException ex) {}
			}
			
			if (geomArr==null || geomArr[0]==null) {
				if (PsDebug.WARNING) PsDebug.warning("error when reading file = "+fileName);
			} else {
				for (int i=0; i<geomArr.length; i++) {
					// Assign geometry a name based on the file name.
					if (fileName != null) {
						String baseName = PsUtil.getFileBaseName(fileName);
						if (geomArr.length == 1)
							geomArr[i].setName(baseName);
						else
							geomArr[i].setName(baseName+"["+String.valueOf(i)+"]");
					}
					// Optionally, convert PgJvxSrc into an element set or point set
					// since PgJvxSrc does not have an info or material panel.
					// 
					// For efficiency, the jv.geom.* data structures may be directly
					// used within the loader. In JavaView, the loaders avoid using
					// classes from jv.geom to allow jvLite not to depend on jv.geom.
					PgGeometryIf geom = null;
					switch (geomArr[i].getType()) {
					case PvGeometryIf.GEOM_POINT_SET:
						geom 	= new PgPointSet(geomArr[i].getDimOfVertices());
						geom.setJvx(geomArr[i]);
						break;
					case PvGeometryIf.GEOM_POLYGON:
						geom 	= new PgPolygon(geomArr[i].getDimOfVertices());
						geom.setJvx(geomArr[i]);
						break;
					case PvGeometryIf.GEOM_POLYGON_SET:
						geom 	= new PgPolygonSet(geomArr[i].getDimOfVertices());
						geom.setJvx(geomArr[i]);
						break;
					case PvGeometryIf.GEOM_ELEMENT_SET:
						geom = new PgElementSet(geomArr[i].getDimOfVertices());
						geom.setJvx(geomArr[i]);
						break;
					default:
						PsDebug.warning("geometry["+i+"] has unknown type = "+geomArr[i].getType());
						// If geometry does not have a recognized type, for example,
						// if this loader parses a geometry file produced with some future
						// version of JavaView, then we constrain to using the PgJvxSrc.
						geom 	= geomArr[i];
					}
					
					// Add geometry to project.
					addGeometry(geom);
					if (i == 0) {
						selectGeometry(geom);
					}
				}
			}
		}
		super.start();
	}

	/** Update this project. */
	public boolean update(Object object) {
		if (PsDebug.NOTIFY) PsDebug.notify("called");
		return super.update(object);
	}
}
