package vgp.tutor.loader;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Tutorial on geometry loaders explains how to write
 * and use an own geometry loader within JavaView.
 * <p>
 * The loader used in the init() method of this applet can be replaced with
 * any of the built-in loaders (jv.loader.*, jvx.loader.*) of JavaView.
 * 
 * @see			vgp.tutor.loader.PjLoader
 * @see			vgp.tutor.loader.PgAbcLoader
 * @author		Konrad Polthier
 * @version		15.12.16, 3.00 revised (kp) Body moved to new class PjLoader.<br>
 * 				08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				22.02.03, 1.15 revised (kp) Check for unknown geometry types.<br>
 *					06.10.02, 1.10 revised (kp) Use codebase when loading a file.<br>
 *					09.11.01, 1.00 created (kp)
 */
public class PaLoader extends PvApplet {
	/**
	 * Interface used by design tools to show properties of applet.
	 * This method returns a list of string arrays, each of length 4 rather than 3
	 * as suggest by Java. The additional string at third position contains
	 * the value of the parameter.
	 * @see			jv.viewer.PvViewer#getParameter(String)
	 */
	public String[][] getParameterInfo() { return PjLoader.m_parmLoader; }
	
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Tutorial on usage of parsing an own geometry file format." + "\r\n";
	}
	/** Return a new allocated project instance. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/**
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjLoader();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaLoader(), args);
	}
}
