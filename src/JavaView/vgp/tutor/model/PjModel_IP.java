package vgp.tutor.model;

import java.awt.*;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel of PjModel with a textfield to load a JavaView built-in model.
 * 
 * @author		Konrad Polthier
 * @version		15.01.00, 1.00 revised (kp)<br>
 *					15.01.00, 1.00 revised (kp)
 */
public class PjModel_IP extends PjProject_IP {
	protected	PjModel					m_pjModel;
	protected	Panel						m_pInfo;
	
	/** Constructor */
	public PjModel_IP() {
		super();
		if (getClass() == PjModel_IP.class) {
			init();
		}
	}
	
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		// start of Model load panel
		m_pInfo = new Panel();
		m_pInfo.setLayout(new BorderLayout());
		add(m_pInfo);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Sample inclusion of the small info panel "+
							 "of project PjImportModel. Type the full "+
							 "name of a local geometry file, or the name "+
							 "relative to the current code base, or the full URL.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjModel = (PjModel)parent;
		m_pInfo.add(m_pjModel.m_import.getInfoPanel(), "Center");
		m_pInfo.validate();
	}
	/**
	 * Here we arrive from outside world of this panel, e.g. if
	 * parent has changed somewhere else.
	 */
	public boolean update(Object anObject) {
		if (m_pjModel==null) {
			if (PsDebug.WARNING) PsDebug.warning("missing parent, setParent not called");
			return false;
		}
		if (anObject == m_pjModel) {
			return true;
		}
		return super.update(anObject);
	}
}
