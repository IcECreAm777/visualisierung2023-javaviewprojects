package vgp.tutor.eventCamera;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Panel;

import jv.project.PvApplet;
import jv.project.PjProject;
import jv.project.PvDisplayIf;
import jv.project.PvViewerIf;
import jv.viewer.PvViewer;

/**
 * Applet studies camera events issued from a JavaView display.
 * In this example the events are used to steer the camera in a second window.
 * <p>
 * The camera events from the right window are used to steer the
 * camera in the left window such that a stereo effect is produced.
 * Note, the viewing direction of the left camera is slightly rotated
 * around the up-vector of the camera by about 6 degrees.
 * 
 * @see			vgp.tutor.eventCamera.PjEventCamera
 * @author		Konrad Polthier
 * @version		08.11.16, 3.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				14.12.03, 2.00 revised (kp) Full revision with better panels.<br>
 *					25.07.00, 1.00 created (kp)
 */
public class PaEventCamera extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "3.00" + "\r\n" +
				 "Applet demonstrates to handle camera events and setup stereo viewing." + "\r\n";
	}
	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects may be selected here.
	 */
	public void run() {
		// super.run();

		// Create viewer for viewing 3d geometries
		PvViewerIf viewer		= super.getViewer();
		PvDisplayIf disp		= super.getDisplay();
		
		// Create and load a project
		PjEventCamera pjStereo = new PjEventCamera();
		((PvViewer)viewer).addProject(pjStereo);
		((PvViewer)viewer).selectProject(pjStereo);

		// Add two displays at top and the project inspector at the bottom.
		setLayout(new BorderLayout());
		Panel pDisplay = new Panel();
		pDisplay.setLayout(new GridLayout(1, 2));
		pDisplay.add((Component)pjStereo.getLeftDisplay());
		pDisplay.add((Component)pjStereo.getRightDisplay());
		add(pDisplay, BorderLayout.CENTER);
		add(pjStereo.getInfoPanel(), BorderLayout.SOUTH);
		
		validate();

		// Explicitly start the applet
		startFromThread();
	}
	/**
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return null;
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaEventCamera(), args);
	}
}
