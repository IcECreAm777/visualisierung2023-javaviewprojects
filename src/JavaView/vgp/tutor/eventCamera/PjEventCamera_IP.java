package vgp.tutor.eventCamera;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel of ode demonstration with text field to edit ode expression.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 2.10 revised (kp) Doc and notice added.<br>
 *					14.12.03, 2.00 revised (kp) Full revision with different projection types.<br>
 *					25.07.00, 1.00 created (kp)
 */
public class PjEventCamera_IP extends PjProject_IP implements ActionListener, ItemListener {
	protected	PjEventCamera			m_pjEvent;
	protected	PsPanel					m_pSlider;
	protected	CheckboxGroup			m_gStereoType;
	protected	Checkbox					m_cCrossEye;
	protected	Checkbox					m_cParallelEye;
	protected	Button					m_bReset;

	/** Constructor */
	public PjEventCamera_IP() {
		super();
		if (getClass() == PjEventCamera_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		// Panel for checkboxes
		PsPanel pCheck = new PsPanel();
		pCheck.setLayout(new GridLayout(1, 2));
		{
			m_gStereoType	= new CheckboxGroup();
			
			m_cCrossEye		= new Checkbox("Cross Eye", m_gStereoType, true);
			m_cCrossEye.addItemListener(this);
			pCheck.add(m_cCrossEye);
			
			m_cParallelEye	= new Checkbox("Parallel Eye", m_gStereoType, false);
			m_cParallelEye.addItemListener(this);
			pCheck.add(m_cParallelEye);
		}
		add(pCheck);
		
		// Panel for slider
		m_pSlider = new PsPanel();
		add(m_pSlider);

		// buttons at bottom
		Panel m_pBottomButtons = new Panel();
		m_pBottomButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		add(m_pBottomButtons);
		m_bReset = new Button("Reset");
		m_bReset.addActionListener(this);
		m_pBottomButtons.add(m_bReset);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Demo project shows how to handle camera events issued from "+
							 "a JavaView display. In this example the events are used to "+
							 "steer the camera in a second window. "+
							 "\n"+
							 "The camera events from one window are used to steer the "+
							 "camera in the other window such that a stereo effect is produced. "+
							 "Note, the viewing direction of the left camera is slightly rotated "+
							 "around the up-vector of the camera by about 6 degrees.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjEvent = (PjEventCamera)parent;
		m_pSlider.removeAll();
		m_pSlider.add(m_pjEvent.m_parallax.getInfoPanel());
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (m_pjEvent == event) {
			switch (m_pjEvent.getStereoType()) {
			case PjEventCamera.STEREO_CROSS:
				m_gStereoType.setSelectedCheckbox(m_cCrossEye);
				break;
			case PjEventCamera.STEREO_PARALLEL:
				m_gStereoType.setSelectedCheckbox(m_cParallelEye);
				break;
			}
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjEvent==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjEvent.reset();
		}
	}
	public void itemStateChanged(ItemEvent event) {
		if (PsDebug.NOTIFY) PsDebug.notify("entered");
		Object source = event.getSource();
		if (source == m_cCrossEye) {
			m_pjEvent.setStereoType(PjEventCamera.STEREO_CROSS);
			m_pjEvent.updateDisplay(m_pjEvent.m_currDisp);
		} else if (source == m_cParallelEye) {
			m_pjEvent.setStereoType(PjEventCamera.STEREO_PARALLEL);
			m_pjEvent.updateDisplay(m_pjEvent.m_currDisp);
		}
	}
}

