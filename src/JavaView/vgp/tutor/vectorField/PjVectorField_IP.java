package vgp.tutor.vectorField;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.objectGui.PsTabPanel;
import jv.project.PjProject_IP;

/**
 * Info panel of tutorial project on gradient and co-gradient vector fields.
 *
 * @author		Konrad Polthier
 * @version		11.11.02, 1.01 revised (ep) Corrected hint text.<br>
 *					18.09.99, 1.00 created (kp)
 */
public class PjVectorField_IP extends PjProject_IP implements ActionListener, ItemListener {
	protected	PjVectorField				m_pjVectorfield;
	protected	PsTabPanel					m_tabPanel;
	protected	PsPanel						m_pPotential;

	protected	CheckboxGroup				m_gField;
	protected	Checkbox						m_cGradient;
	protected	Checkbox						m_cRotation;

	/** Constructor */
	public PjVectorField_IP() {
		super();
		if (getClass() == PjVectorField_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		m_pPotential = new PsPanel();
		{
			Panel pField = new Panel();
			pField.setLayout(new GridLayout(1, 2));
			{
				m_gField = new CheckboxGroup();
				m_cGradient	= new Checkbox("Gradient", m_gField, true);
				m_cGradient.addItemListener(this);
				pField.add(m_cGradient);
				m_cRotation		= new Checkbox("Rotation", m_gField, false);
				m_cRotation.addItemListener(this);
				pField.add(m_cRotation);
			}
			m_pPotential.add(pField);
		}
		m_tabPanel = new PsTabPanel();
		add(m_tabPanel);
}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Usage: Press 'a' and click into display to add a singularity. "+
							 "Press 'p' while dragging one of the red singularities. "+
							 "Use Method->Mark->MarkVertices and "+
							 "Method->Delete->MarkedVertices to remove singularities.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjVectorfield = (PjVectorField)parent;
		m_tabPanel.init(); // reset tab
		{
			m_pPotential.add(m_pjVectorfield.m_radius.getInfoPanel());
			m_tabPanel.addPanel("Potential", m_pPotential);
		}
		if (m_pjVectorfield.m_geom!=null && m_pjVectorfield.m_geom.getControlPanel()!=null) {
			m_tabPanel.addPanel("Grid", m_pjVectorfield.m_geom.getControlPanel());
		}
		if (m_pjVectorfield.m_vf!=null) {
			PsPanel pInfo = m_pjVectorfield.m_vf.getInfoPanel();
			if (pInfo != null) {
				pInfo.setBorderType(PsPanel.BORDER_NONE);
				m_tabPanel.addPanel("Vector Info", pInfo);
			}
		}
		// Refresh card layout and show named panel
		m_tabPanel.validate();
		m_tabPanel.setVisible("Potential");
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("PjVectorField_IP.update: called");
		if (event == m_pjVectorfield) {
			if (m_gField.getSelectedCheckbox()==m_cGradient &&
				 m_pjVectorfield.getFieldType()!=PjVectorField.GRADIENT)
				m_gField.setSelectedCheckbox(m_cRotation);
			else if (m_gField.getSelectedCheckbox()==m_cRotation &&
				 m_pjVectorfield.getFieldType()!=PjVectorField.ROTATION)
				m_gField.setSelectedCheckbox(m_cGradient);
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjVectorfield==null)
			return;
		// Object source = event.getSource();
	}
	/**
	 * Handle item state events invoked from choices, checkboxes, list items.
	 */
	public void itemStateChanged(ItemEvent event) {
		if (m_pjVectorfield == null)
			return;
		Object source = event.getSource();
		if (source == m_cGradient) {
			m_pjVectorfield.setFieldType(PjVectorField.GRADIENT);
			m_pjVectorfield.update(this);
		} else if (source == m_cRotation) {
			m_pjVectorfield.setFieldType(PjVectorField.ROTATION);
			m_pjVectorfield.update(this);
		}
	}
}
