package vgp.tutor.vectorField;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Tutorial applet on vector fields on planar grids and curved surfaces.
 * 
 * @see			vgp.tutor.vectorField.PaVectorField
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				18.09.99, 1.00 created (kp)
 */
public class PaVectorField extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
		       "Author: "		+ "Konrad Polthier" + "\r\n" +
		       "Version: "	+ "2.00" + "\r\n" +
				 "Gradient and Co-Gradient Vectorfields on Triangulations" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjVectorField();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaVectorField(), args);
	}
}
