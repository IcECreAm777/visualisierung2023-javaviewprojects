package vgp.tutor.polygonSet;

import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for polygon set demo.
 * 
 * @author		Konrad Polthier
 * @version		16.12.16, 1.00 created (kp)
 */
public class PjPolygonSet_IP extends PjProject_IP {

	/** Constructor */
	public PjPolygonSet_IP() {
		super();
		if (getClass() == PjPolygonSet_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Tutorial project illustrates usage of class PgPolygonSet: here "+
							 "we compute meridians with different length of a torus. The "+
							 "color of each polygon changes continuously and it ends with an arrow.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object anObject) {
		if (anObject == m_project) {
			return true;
		}
		return super.update(anObject);
	}
}

