package vgp.tutor.flows;

import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

import jvx.project.PjWorkshop_IP;

/**
 * Panel for seeds project.
 * @author		Konrad Polthier
 * @version		15.12.16, 1.00 created (kp)
 */
public class PjSeeds_IP extends PjProject_IP {
	protected	PjSeeds				m_pjSeeds;
	protected	PjWorkshop_IP		m_wsSeeds_IP;
	
	/** Constructor */
	public PjSeeds_IP() {
		super();

		if (getClass() == PjSeeds_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
	}		
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		return "";
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjSeeds = (PjSeeds)m_project;

		m_wsSeeds_IP	= (PjWorkshop_IP)m_pjSeeds.m_wsSeeds.newInspector(PsPanel.INFO_EXT);
		m_wsSeeds_IP.removeTitle();
		// m_infoPanel.setTitle("Options");
		add(m_wsSeeds_IP);
		
		setNotice(m_wsSeeds_IP.getNotice());
	}
	/**
	* Here we arrive from outside world of this panel, e.g. if
	* project has changed somewhere else and must update its panel. Such an update
	* is automatically by superclasses of PjProject.
	*/
	public boolean update(Object event) {
		if (m_pjSeeds == null) {
			if (PsDebug.WARNING) PsDebug.warning("missing parent, setParent not called");
			return false;
		}
		return super.update(event);
	}
}


