package vgp.tutor.flows;

import java.awt.Color;

import jv.geom.PgElementSet;
import jv.loader.PjImportModel;
import jv.object.PsConfig;
import jv.object.PsDebug;
import jv.object.PsUtil;
import jv.project.PjProject;
import jv.project.PvViewerIf;

import jvx.vector.PwSeeds;

/**
 * Demo project for showing particle flows on surfaces.
 * 
 * @see			jvx.vector.PwSeeds
 * @author		Konrad Polthier
 * @version		15.12.16, 1.00 created (kp) Based on run() code of PaSeeds.<br>
 */
public class PjSeeds extends PjProject {
	/** Test model with vector fields. */
	protected	static final String	m_modelName		= "models/vector/brezelVector.jvx";
	/** Seeds workshop. */
	protected	PwSeeds					m_wsSeeds;
	/** Main geometry. */
	protected	PgElementSet			m_geom;
	

	public PjSeeds() {
		super("Seeds Particle Flow Demo");

		m_geom = new PgElementSet(3);
		// m_geom.setParent(this);

		m_wsSeeds = new PwSeeds();

		if (getClass() == PjSeeds.class) {
			init();
		}
	}
	public void init() {
		super.init();
	}
	public void start() {
		if (PsDebug.NOTIFY) PsDebug.notify("called");

		PvViewerIf viewer = getViewer();
		if (viewer != null) {
			String fileName = PsConfig.getCodeBase()+m_modelName;
			PjImportModel model = new PjImportModel();
			PgElementSet geo = null;
			if (!model.load(fileName)) {
				PsDebug.message("PjModel.start(): failed to load geometry from file "+fileName);
				return;
			}
			m_geom.copy((PgElementSet)model.getGeometry());
			m_geom.setName(PsUtil.getFileBaseName(m_modelName));
			
			m_geom.setGlobalVectorColor(Color.black);
			m_geom.setGlobalElementColor(Color.white);
			m_geom.showVectorFields(false);
			
			//Set visual properties of element set
			m_geom.showEdges(false);
			m_geom.showElementTexture( false );
			//m_geom.showElements(false);
			m_geom.setVisible(true);
			m_geom.update(null);
			
			addGeometry(m_geom);

			m_wsSeeds.setGeometry(m_geom);
			m_wsSeeds.setDisplay(viewer.getDisplay());
			/*
				// Alternatively, show workshop dialog instead of workshop_IP.
				PsDialog dialog = new PjWorkshop_Dialog(false);
				dialog.setParent(m_wsSeeds);
				dialog.update(m_wsSeeds);
				dialog.setVisible(true);
			*/
		}
		super.start();
	}
}

