package vgp.tutor.flows;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Demo applet for showing particle flows on surfaces.
 * 
 * @see			vgp.tutor.flows.PjSeeds
 * @author		Andreas Haferburg, Konrad Polthier
 * @version		15.12.16, 3.00 revised (kp) Body moved to PjSeeds.<br>
 * 				08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 */
public class PaSeeds extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Andreas Haferburg" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Visualize Vectorfields on Triangulations using Seeds" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjSeeds();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaSeeds(), args);
	}
}
