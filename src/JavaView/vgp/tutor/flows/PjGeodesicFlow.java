package vgp.tutor.flows;

import java.awt.Color;

import jv.geom.PgElementSet;
import jv.loader.PjImportModel;
import jv.object.PsConfig;
import jv.object.PsDebug;
import jv.project.PjProject;
import jv.project.PvViewerIf;

import jvx.geom.PwGeodesicFlow;

/**
 * Demo project for showing geodesic flows on surfaces.
 * 
 * @see			jvx.geom.PwGeodesicFlow
 * @author		Konrad Polthier
 * @version		15.12.16, 1.00 created (kp) Based on run() code of PaGeodesicFlow.<br>
 */
public class PjGeodesicFlow extends PjProject {
	/** Test model with vector fields. */
	protected	static final String	m_modelName		= "models/vector/brezelVector.jvx";
	/** Seeds workshop. */
	protected	PwGeodesicFlow			m_wsGeodesicFlow;
	/** Main geometry. */
	protected	PgElementSet			m_geom;

	public PjGeodesicFlow() {
		super("Geodesic Flow Demo");

		m_geom = new PgElementSet(3);
		// m_geom.setParent(this);

		m_wsGeodesicFlow = new PwGeodesicFlow();

		if (getClass() == PjGeodesicFlow.class) {
			init();
		}
	}
	public void init() {
		super.init();
	}
	public void start() {
		if (PsDebug.NOTIFY) PsDebug.notify("called");

		PvViewerIf viewer = getViewer();
		if (viewer != null) {
			String fileName = PsConfig.getCodeBase()+m_modelName;
			PjImportModel model = new PjImportModel();
			PgElementSet geo = null;
			if (!model.load(fileName)) {
				PsDebug.message("PjModel.start(): failed to load geometry from file "+fileName);
				return;
			}
			geo = (PgElementSet)model.getGeometry().clone();
			
			geo.setGlobalVectorColor(Color.black);
			geo.setGlobalElementColor(Color.white);
			geo.showVectorFields(false);
			
			//Set visual properties of element set
			geo.showEdges(false);
			geo.showElementTexture( false );
			//geo.showElements(false);
			geo.setVisible(true);
			geo.update(null);
			
			addGeometry(geo);
			
			m_wsGeodesicFlow.setGeometry(geo);
			m_wsGeodesicFlow.setDisplay(viewer.getDisplay());
			/*
				// Alternatively, show workshop dialog instead of workshop_IP.
				PsDialog dialog = new PjWorkshop_Dialog(false);
				dialog.setParent(workshop);
				dialog.update(workshop);
				dialog.setVisible(true);
			*/
		}
		super.start();
	}
}

