package vgp.tutor.pick;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for pick demo.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 1.20 revised (kp) Doc and notice added.<br>
 *					15.09.99, 1.10 revised (kp) Converted to AWT1.1 event model.<br>
 *					00.00.99, 1.00 created (kp)
 */
public class PjPickEvent_IP extends PjProject_IP implements ActionListener, ItemListener {
	protected	PjPickEvent				m_pjCurve;
	protected	PsPanel					m_pBounds;
	protected	PsPanel					m_pTube;
	protected	Button					m_bReset;
	protected	Checkbox					m_cShowTorus;
	protected	Checkbox					m_cShowTube;

	/** Constructor */
	public PjPickEvent_IP() {
		super();
		if (getClass() == PjPickEvent_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
		
		addLine(1);

		m_pTube = new PsPanel();
		add(m_pTube);
		
		addLine(1);

		Panel m_pShow = new Panel(new FlowLayout());
		add(m_pShow);
		{
			m_cShowTorus = new Checkbox("Show Torus");
			m_cShowTorus.addItemListener(this);
			m_pShow.add(m_cShowTorus);
			m_cShowTube = new Checkbox("Show Tube");
			m_cShowTube.addItemListener(this);
			m_pShow.add(m_cShowTube);
		}
		// buttons at bottom
		Panel pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		m_bReset = new Button ("Reset");
		m_bReset.addActionListener(this);
		pBottomButtons.add(m_bReset);
		add(pBottomButtons);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Use initial pick (key-i and mouse-click) to draw a tubified curve "+
							 "on the torus. Note, picking on the display background is valid too. "+
							 "Rotate geometry to see effect of picking on surface or background.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjCurve = (PjPickEvent)parent;
		m_pTube.add(m_pjCurve.m_tube.getControlPanel());
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object anObject) {
		if (PsDebug.NOTIFY) PsDebug.notify("PjPickEvent_IP.update: isShowing = "+isShowing());
		if (anObject == m_project) {
			m_cShowTorus.setState(m_pjCurve.m_bShowTorus);
			m_cShowTube.setState(m_pjCurve.m_bShowTube);
			return true;
		}
		return super.update(anObject);
	}

	public void actionPerformed(ActionEvent event) {
		if (m_pjCurve==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			if (PsDebug.NOTIFY) PsDebug.notify("PjPickEvent_IP.actionPerformed: reset");
			m_pjCurve.init();
			m_pjCurve.removeGeometry(m_pjCurve.m_knot);
			m_pjCurve.addGeometry(m_pjCurve.m_knot);
			if (m_pjCurve.m_bShowTube == true) {
				m_pjCurve.removeGeometry(m_pjCurve.m_tube);
				m_pjCurve.addGeometry(m_pjCurve.m_tube);
			}
			if (m_pjCurve.m_bShowTorus == true)
				m_pjCurve.selectGeometry(m_pjCurve.m_torus);
			m_pjCurve.m_knot.update(m_pjCurve.m_knot);
		}
	}
	public void itemStateChanged(ItemEvent event) {
		if (m_pjCurve==null)
			return;
		Object source = event.getSource();
		if (source == m_cShowTorus) {
			if (PsDebug.NOTIFY) PsDebug.notify("PjPickEvent_IP.itemStateChanged(): switch show torus");
			m_pjCurve.m_bShowTorus = m_cShowTorus.getState();
			if (m_pjCurve.m_bShowTorus == true) {
				m_pjCurve.addGeometry(m_pjCurve.m_torus);
			} else {
				m_pjCurve.removeGeometry(m_pjCurve.m_torus);
			}
		} else if (source == m_cShowTube) {
			if (PsDebug.NOTIFY) PsDebug.notify("PjPickEvent_IP.itemStateChanged(): switch show tube");
			m_pjCurve.m_bShowTube = m_cShowTube.getState();
			if (m_pjCurve.m_bShowTube == true) {
				m_pjCurve.addGeometry(m_pjCurve.m_tube);
			} else {
				m_pjCurve.removeGeometry(m_pjCurve.m_tube);
			}
		}
	}
}

