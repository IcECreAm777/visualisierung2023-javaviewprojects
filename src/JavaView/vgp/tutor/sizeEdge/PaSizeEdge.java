package vgp.tutor.sizeEdge;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet shows usage of individual edge size of element edges.
 * 
 * @see			vgp.tutor.sizeEdge.PjSizeEdge
 * @author		Konrad Polthier
 * @version		08.11.16, 3.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				04.11.06, 2.00 revised (kp) Moved to vgp.tutor.<br>
 *					05.07.06, 1.00 created (kp) Based on similar applet vgp.tutor.sizeEdge.
 */
public class PaSizeEdge extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "3.00" + "\r\n" +
				 "Applet shows usage of individual color and size of element edges" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjSizeEdge();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaSizeEdge(), args);
	}
}
