package vgp.tutor.sizeEdge;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsConfig;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel allows to control the dynamic change of edge thickness.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 2.10 revised (kp) Doc and notice added.<br>
 *					04.11.06, 2.00 revised (kp) Moved to vgp.tutor.<br>
 *					05.07.06, 1.00 created (kp) Based on similar applet vgp.tutor.sizeEdge.
 */
public class PjSizeEdge_IP extends PjProject_IP implements ActionListener, ItemListener { 
	/** Parent project. */
	protected	PjSizeEdge				m_pjSizeEdges;
	/** Determines if individual edges colors are shown. */
	protected	Checkbox					m_cColors;
	/** Determines if individual edges sizes are shown. */
	protected	Checkbox					m_cSizes;
	/** Determines if individual edges labels are shown. */
	protected	Checkbox					m_cLabels;
	/**
	 * Determines if individual edges colors are computed from varying edge size
	 * or fixed edge index.
	 */
	protected	Choice					m_cColorType;
	/** Reset the project and participating geometries. */
	protected	Button					m_bReset;
	/** Run and stop the animation. */
	protected	Button					m_bRun;
	/** Panel contains all sliders of the project. */
	protected	PsPanel					m_pSlider;
	/** Save background color of button m_bRun as long as minimization is running. */
	private		Color						m_bRunColor;

	/** Constructor */
	public PjSizeEdge_IP() {
		super();
		if (getClass() == PjSizeEdge_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		// panel for show switches
		Panel pShow = new Panel(new BorderLayout());
		add(pShow);
		{
			PsPanel pHeader = new PsPanel(new BorderLayout());
			{
				Label lTitle = pHeader.getTitle(PsConfig.getMessage(true, 81000, "Show"), PsConfig.getFont(PsConfig.FONT_HEADER2));
				pHeader.add(lTitle, BorderLayout.NORTH);
			}
			pShow.add(pHeader, BorderLayout.WEST);

			Panel pSwitches = new Panel(new GridLayout(2, 2));

			m_cColors = new Checkbox(PsConfig.getMessage(true, 84000, "Colors"));
			m_cColors.addItemListener(this);
			pSwitches.add(m_cColors);
			
			m_cSizes = new Checkbox(PsConfig.getMessage(true, 84000, "Sizes"));
			m_cSizes.addItemListener(this);
			pSwitches.add(m_cSizes);

			m_cColorType = new Choice();
			m_cColorType.add("Color from Size");
			m_cColorType.add("Color from Index");
			m_cColorType.addItemListener(this);
			pSwitches.add(m_cColorType);

			m_cLabels = new Checkbox(PsConfig.getMessage(true, 84000, "Labels"));
			m_cLabels.addItemListener(this);
			pSwitches.add(m_cLabels);

			pShow.add(pSwitches, BorderLayout.CENTER);
		}

		m_pSlider = new PsPanel(new GridLayout(2, 1));
		add(m_pSlider);

		// buttons at bottom
		Panel pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		add(pBottomButtons);
		m_bRun	= new Button("Run");
		m_bRun.addActionListener(this);
		pBottomButtons.add(m_bRun);
		m_bReset	= new Button("Reset");
		m_bReset.addActionListener(this);
		pBottomButtons.add(m_bReset);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Changes the individual size factor of each edge slightly "+
			 "back and forth with given random initial speed. "+
			 "The size of each edge is globalEdgeSize*individualEdgeSize. "+
			 "The overall speed of the animation may be adjusted.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjSizeEdges = (PjSizeEdge)parent;
		m_pSlider.add(m_pjSizeEdges.m_size.getInfoPanel());
		m_pSlider.add(m_pjSizeEdges.m_speed.getInfoPanel());
			
		validate();
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (m_pjSizeEdges == event) {
			PsPanel.setState(m_cColors, m_pjSizeEdges.m_bShowEdgeColors);
			PsPanel.setState(m_cSizes, m_pjSizeEdges.m_bShowEdgeSizes);
			PsPanel.setState(m_cLabels, m_pjSizeEdges.m_bShowEdgeLabels);
			PsPanel.select(m_cColorType, m_pjSizeEdges.m_colorType);
			return true;			
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjSizeEdges==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjSizeEdges.init();			
			m_pjSizeEdges.start();
			m_pjSizeEdges.update(m_pjSizeEdges);
		} else if (source == m_bRun) {
			if (m_bRun.getLabel().equals("Run")) {
				m_bRunColor = m_bRun.getBackground();
				m_bRun.setBackground(Color.red);
				m_bRun.setLabel("Stop");
				m_pjSizeEdges.startAnim();
			} else {
				if (m_bRunColor != null)
					m_bRun.setBackground(m_bRunColor);
				m_bRun.setLabel("Run");
				m_pjSizeEdges.stopAnim();
			}
		}
	}
	/**
	 * @version		08.07.06, 1.00 created (kp)
	 */
	public void itemStateChanged(ItemEvent event) {
		Object source = event.getSource();
		if (source == m_cColors) {
			m_pjSizeEdges.showEdgeColors(m_cColors.getState());
		} else if (source == m_cSizes) {
			m_pjSizeEdges.showEdgeSizes(m_cSizes.getState());
		} else if (source == m_cLabels) {
			m_pjSizeEdges.showEdgeLabels(m_cLabels.getState());
		} else if (source == m_cColorType) {
			m_pjSizeEdges.setColorType(m_cColorType.getSelectedIndex());
		} else
			return;
		m_pjSizeEdges.update(m_pjSizeEdges);
	}
}
