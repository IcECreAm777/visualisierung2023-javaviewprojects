package vgp.tutor.ruler;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Controls to use a ruler to measure distances in a display.
 *
 * @author		Steve Dugaro
 * @version		23.10.16, 1.10 revised (kp) Doc and notice added.<br>
 *					28.09.00, 1.00 created (spd)
 */
public class PjRuler_IP extends PjProject_IP implements ActionListener {
	protected	PjRuler				m_pjRuler;
	protected   Button				m_bDone;
	
	protected	Button				m_bReset;
	protected   Panel					m_pRuler;

	/** Constructor */
	public PjRuler_IP() {
		super();
		
		if (getClass() == PjRuler_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		// addNotice(getNotice());

		m_pRuler = new PsPanel();
		add(m_pRuler);
		
		addLine(1);

		// buttons at bottom
		Panel m_pBottomButtons = new Panel();
		m_pBottomButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		{
			m_bDone	= new Button("Done");
			m_bDone.addActionListener(this);
			m_pBottomButtons.add(m_bDone);

			m_bReset	= new Button("Reset");
			m_bReset.addActionListener(this);
			m_pBottomButtons.add(m_bReset);
		}
		add(m_pBottomButtons);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Use a ruler to measure horizontal distances in a display. "+
							 "This ruler class is also used to created the JavaView axes.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjRuler = (PjRuler)parent;
		// this is where the logic body of the applet is passed through as the parent of this info panel
		// => we make the assignment here to fill the containment of it /win the info panel.
		// m_x&y return their self contained info panels... i.e. slider & text & label.
		// having the parent we can access its members to add to this info panel.
		PsPanel ip		= m_pjRuler.m_ruler.getControlPanel();
		ip.setBorderType(PsPanel.BORDER_NONE);
		m_pRuler.add(ip);
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (m_pjRuler == event) {
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjRuler==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjRuler.init();
			m_pjRuler.m_ruler.update(m_pjRuler.m_ruler);
		} else if (source == m_bDone) {
		}
	}
}
