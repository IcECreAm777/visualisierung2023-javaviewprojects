package vgp.tutor.ruler;

import jv.object.PsDebug;
import jv.project.PvCameraIf;
import jv.project.PvDisplayIf;
import jv.project.PvPickEvent;
import jv.project.PjProject;
import jv.vecmath.PdVector;

import jv.thirdParty.ruler.PgRuler;

/**
 * Use a ruler to measure horizontal distances in a display.
 *
 * @author		Steve Dugaro
 * @version		28.09.00, 1.00 created (spd)
 */
public class PjRuler extends PjProject {
	protected	PgRuler		m_ruler;
	protected	double		m_xStart;
	protected	double		m_yStart;

	public PjRuler() {
		super("Ruler Demo");
		m_ruler = new PgRuler(3);
		m_ruler.setRulerName("Ruler"); 

		if (getClass() == PjRuler.class)
		  init();
	}
	public void init() {
		super.init();
		m_ruler.init(); //initialize all default instance variables
	}
	public void start() {
		if (PsDebug.NOTIFY) PsDebug.notify("called");
		addGeometry(m_ruler);
		selectGeometry(m_ruler);
		PvDisplayIf disp = getDisplay();
		if (disp != null) {
			disp.showGrid(true);
			disp.selectCamera(PvCameraIf.CAMERA_ORTHO_XY);		// project onto xy-plane
			disp.setMajorMode(PvDisplayIf.MODE_DISPLAY_PICK);	// force picking of initial point
		}
		super.start();
	}
	public void pickDisplay(PvPickEvent pos) {
		m_ruler.setOrigin(pos.getVertex());
	}
	public void dragDisplay(PvPickEvent pos) {
		// dynamically update all the params
		PdVector viewDir = getDisplay().getCamera().getViewDir();
		m_ruler.setEndPoint(pos.getVertex(), viewDir);
		m_ruler.update(m_ruler);
	}

	/**
	 * Update the class whenever a child has changed.
	 * Method is usually invoked from the children.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("called");
		if (event == null) {	
			//return true;
		} 
		return false;
	}    
	
}

