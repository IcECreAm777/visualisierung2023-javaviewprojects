package vgp.tutor.ruler;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet demonstrates usage as ruler to measure in a scene.
 * 
 * @see			vgp.tutor.ruler.PjRuler
 * @author		Steve Dugaro
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				04.05.00, 1.00 created (spd)
 */
public class PaRuler extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Steve Dugaro" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Applet demonstrates the use of a ruler to measure on a display." + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjRuler();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaRuler(), args);
	}
}
