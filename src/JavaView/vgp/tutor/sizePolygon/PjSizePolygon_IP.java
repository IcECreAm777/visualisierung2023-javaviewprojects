package vgp.tutor.sizePolygon;

import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel to control the dynamic change of edge thickness.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 2.20 revised (kp) Doc and notice added.<br>
 *					04.11.06, 2.10 revised (kp) Moved to vgp.tutor.<br>
 *					05.07.06, 2.00 created (kp) Copied from previous named PaSizeEdge.<br>
 *					07.02.04, 1.00 created (kp) 
 */
public class PjSizePolygon_IP extends PjProject_IP implements ActionListener { 
	/** Parent project. */
	protected	PjSizePolygon			m_pjSizePolygons;
	/** Reset the project and participating geometries. */
	protected	Button					m_bReset;
	/** Run and stop the animation. */
	protected	Button					m_bRun;
	/** Panel contains all sliders of the project. */
	protected	PsPanel					m_pSlider;
	/** Save background color of button m_bRun as long as minimization is running. */
	private		Color						m_bRunColor;

	/** Constructor */
	public PjSizePolygon_IP() {
		super();
		if (getClass() == PjSizePolygon_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		m_pSlider = new PsPanel(new GridLayout(2, 1));
		add(m_pSlider);

		// buttons at bottom
		Panel pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		add(pBottomButtons);
		m_bRun	= new Button("Run");
		m_bRun.addActionListener(this);
		pBottomButtons.add(m_bRun);
		m_bReset	= new Button("Reset");
		m_bReset.addActionListener(this);
		pBottomButtons.add(m_bReset);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Changes the individual size factor of each edge slightly "+
				 "back and forth with given random initial speed. "+
				 "The size of each edge is globalEdgeSize*individualEdgeSize. "+
				 "The overall speed of the animation may be adjusted.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjSizePolygons = (PjSizePolygon)parent;
		m_pSlider.add(m_pjSizePolygons.m_speed.getInfoPanel());
		m_pSlider.add(m_pjSizePolygons.m_discr.getInfoPanel());
			
		validate();
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (m_pjSizePolygons == event) {
			return true;			
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjSizePolygons==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjSizePolygons.init();			
			m_pjSizePolygons.start();
			m_pjSizePolygons.update(m_pjSizePolygons);
		} else if (source == m_bRun) {
			if (m_bRun.getLabel().equals("Run")) {
				m_bRunColor = m_bRun.getBackground();
				m_bRun.setBackground(Color.red);
				m_bRun.setLabel("Stop");
				m_pjSizePolygons.startAnim();
			} else {
				if (m_bRunColor != null)
					m_bRun.setBackground(m_bRunColor);
				m_bRun.setLabel("Run");
				m_pjSizePolygons.stopAnim();
			}
		}
	}
}
