package vgp.tutor.rivara;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * This applet demonstrates the Rivara refinement process.
 * 
 * @see			vgp.tutor.rivara.PjRivara
 * @author		Axel Friedrich
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				30.09.99, 1.00 created (af)
 */
public class PaRivara extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Axel Friedrich" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Applet demonstrates adaptive refinement of surfaces" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjRivara();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaRivara(), args);
	}
}
