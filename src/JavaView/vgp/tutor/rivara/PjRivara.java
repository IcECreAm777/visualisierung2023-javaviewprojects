package vgp.tutor.rivara;

import jv.geom.PgElementSet;
import jv.object.PsDebug;
import jv.project.PvPickEvent;
import jv.project.PjProject;
import jvx.geom.PwRivaraBisection;

/**
 * Project demonstrates the Rivara refinement.
 * 
 * @author		Axel Friedrich
 * @version		30.09.99, 1.00 created (af)
 */
public class PjRivara extends PjProject {
	protected	boolean				m_bShowTorus;
	protected	PgElementSet		m_torus;
	protected	PwRivaraBisection m_bisection;

	public PjRivara() {
		super("Rivara Refinement Demo");
		m_torus = new PgElementSet(3);
		m_torus.setName("Torus");
		m_bisection = new PwRivaraBisection();

		if (getClass() == PjRivara.class) {
			init();
		}
	}
	public void init() {
		super.init();
		m_bShowTorus	= true;

		m_torus.init();
		m_torus.showElements(false);
		m_torus.computeTorus(10, 10, 2., 1.);
		m_torus.makeQuadrBnd(10, 10);
		m_torus.close();
		PgElementSet.triangulate(m_torus);
	}
	public void start() {
		addGeometry(m_torus);
		selectGeometry(m_torus);
		super.start();
	}
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("called.");
		return super.update(event);
	}
	public void pickInitial(PvPickEvent pos) {
		if (!(pos.getGeometry() instanceof PgElementSet)) {
			if (PsDebug.WARNING) PsDebug.warning("geometry is not instance of PgElementSet.");
			return;
		}
		PgElementSet geom = (PgElementSet)pos.getGeometry();
		int elemInd = pos.getElementInd();
		if (elemInd<0 || geom.getNumElements()<=elemInd) {
			if (PsDebug.WARNING) PsDebug.warning("element index out of range.");
			return;
		}
		// Initialize the array of selected elements with a single element.
		int [] elements = new int[1];
		elements[0] = elemInd;

		// Call the Rivara refinement method on the torus.
		m_bisection.rivaraRefinement(geom, elements);

		geom.update (geom);
	}
}

