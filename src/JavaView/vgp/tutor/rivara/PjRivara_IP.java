package vgp.tutor.rivara;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jv.object.PsDebug;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for pick demo.
 * 
 * @author		Axel Friedrich
 *	@version		23.10.16, 1.10 revised (kp) Doc and notice added.<br>
 *					30.09.99, 1.00 created (af)
 */
public class PjRivara_IP extends PjProject_IP implements ActionListener {
	protected	PjRivara				m_pjRefine;
	protected	Button				m_bReset;

	/** Constructor */
	public PjRivara_IP() {
		super();
		if (getClass() == PjRivara_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
		
		// buttons at bottom
		Panel pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		m_bReset = new Button ("Reset");
		m_bReset.addActionListener(this);
		pBottomButtons.add(m_bReset);
		add(pBottomButtons);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Mesh refinement with the Rivara bisection algorithm. "+
							 "Use initial pick (press key-i and drag with pressed left-mouse). "+
							 "Picked triangles are refined according to the Rivara algorithm. "+
							 "Typically the effect is local, though global effects may occur "+
							 "but in a controlled way.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjRefine = (PjRivara)parent;
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object anObject) {
		if (PsDebug.NOTIFY) PsDebug.notify("PjRivara_IP.update: isShowing = "+isShowing());
		if (anObject == m_project) {
			return true;
		}
		return super.update(anObject);
	}

	public void actionPerformed(ActionEvent event) {
		if (m_pjRefine==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			if (PsDebug.NOTIFY) PsDebug.notify("PjRivara_IP.actionPerformed: reset");
			m_pjRefine.init();
			m_pjRefine.m_torus.update(m_pjRefine.m_torus);
		}
	}
}

