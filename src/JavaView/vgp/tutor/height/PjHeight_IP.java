package vgp.tutor.height;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel of project to select among different scalar fields on the surface.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 1.40 revised (kp) Doc and notice added.<br>
 *					23.10.99, 1.30 revised (kp) Variable m_scalarName removed.<br>
 *					20.08.99, 1.20 revised (kp) Textfield replaced with Choice to select field.<br>
 *					15.08.99, 1.10 revised (kp) Updated to AWT1.1<br>
 *					00.00.98, 1.00 created (kp)
 */
public class PjHeight_IP extends PjProject_IP implements ItemListener {
	protected	PjHeight					m_pjHeight;
	protected	Choice					m_scalarChoice;

	/** Constructor */
	public PjHeight_IP() {
		super();
		if (getClass() == PjHeight_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		Panel surfPanel = new Panel();
		{
			surfPanel.setLayout(new GridLayout(1,2));
			surfPanel.add(new Label("Scalar Field: "));

			m_scalarChoice = new Choice();
			m_scalarChoice.addItemListener(this);
			surfPanel.add(m_scalarChoice);
		}
		add(surfPanel);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Demo project colors a surface from a scalar function.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjHeight = (PjHeight)parent;
		m_scalarChoice.removeAll();
		String [] listScalarFields = m_pjHeight.m_scalarFields;
		if (listScalarFields != null) {
			int len = listScalarFields.length;
			for (int i=0; i<len; i++)
				m_scalarChoice.add(listScalarFields[i]);
		}
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (event == m_pjHeight) {
			m_scalarChoice.select(m_pjHeight.getScalarName());
		}
		return super.update(event);
	}
	/**
	 * Handle item state events invoked from choices, checkboxes, list items.
	 */
	public void itemStateChanged(ItemEvent event) {
		if (m_pjHeight==null)
			return;
		Object source = event.getSource();
		if (source == m_scalarChoice) {
			m_pjHeight.setScalarName(m_scalarChoice.getSelectedItem());
		}
	}
}

