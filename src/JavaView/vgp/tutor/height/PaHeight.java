package vgp.tutor.height;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet demonstrates the display of a scalar field on a surface.
 * 
 * @see			vgp.tutor.height.PjHeight
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				30.01.99, 1.00 created (kp)
 */
public class PaHeight extends PvApplet {
	/**
	 * Interface used by design tools to show properties of applet.
	 * This method returns a list of string arrays, each of length 4 rather than 3
	 * as suggest by Java. The additional string at third position contains
	 * the value of the parameter.
	 * @see			jv.viewer.PvViewer#getParameter(String)
	 */
	public String[][] getParameterInfo() { return PjHeight.m_parmHeight; }

	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Applet shows visualization of scalar height fields" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjHeight();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaHeight(), args);
	}
}
