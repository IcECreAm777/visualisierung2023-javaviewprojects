package vgp.tutor.parm;

import jv.function.PuFunction;
import jv.project.PjProject;
import jvx.surface.PgDomainDescr;
import jvx.surface.PgParmSurface;

/**
 * Describes parametrized surfaces over a two dimensional domain.
 * Coordinate functions are given as strings and may be
 * interactively edited. Parameter domain allows adjustments.
 * 
 * @see			jvx.surface.PgDomainDescr
 * @see			jvx.surface.PgParmSurface
 * @author		Konrad Polthier
 * @version		23.10.16, 1.30 revised (kp) Doc and notice added.<br>
 *					03.01.02, 1.20 revised (kp) Adjusted to new superclass of PgParmSurface.<br>
 *					27.08.99, 1.10 revised (kp) Name PgDomain changed to PgDomainDescr.<br>
 *					27.08.98, 1.00 created (kp) from PgParmSurface.
 */
public class PjParmSurface extends PjProject {
	/** Domain of parametrized surface. */
	protected	PgDomainDescr			m_domain;
	/** Function object consisting of functions as strings. */
	protected	PuFunction				m_function;
	/** Parametrized Surface. */
	protected	PgParmSurface			m_parmSurface;

	public PjParmSurface() {
		super("Parametrized Surface Demo");
		int dimOfDomain	= 2;
		int numFunctions	= 3;
		m_domain			= new PgDomainDescr(dimOfDomain);
		m_function		= new PuFunction(dimOfDomain, numFunctions);
		m_parmSurface	= new PgParmSurface(numFunctions);
		m_parmSurface.setName("Kuen Surface");
		m_parmSurface.setParent(this);
		if (getClass() == PjParmSurface.class) {
			init();
		}
	}
	public void init() {
		super.init();
		m_domain.setName("Domain of Kuen");
		m_domain.setMaxSize(-10., -10., 10., 10.);
		m_domain.setSize(-4.5, 0.05, 4.5, Math.PI-.05);
		m_domain.setDiscr(16, 16);
		m_domain.init();
		m_parmSurface.setDomainDescr(m_domain);
		m_function.setName("Functions of Kuen");
		m_function.setExpression(0, "2./(1.+(u*sin(v))^2)*sqrt(1.+u*u)*sin(v)*cos(u-atan(u))");
		m_function.setExpression(1, "2./(1.+(u*sin(v))^2)*sqrt(1.+u*u)*sin(v)*sin(u-atan(u))");
		m_function.setExpression(2, "log(tan(v/2.))+2./(1.+(u*sin(v))^2)*cos(v)");
		m_parmSurface.setFunctionExpr(m_function);
	}
	public void start() {
		m_parmSurface.compute();
		addGeometry(m_parmSurface);
		selectGeometry(m_parmSurface);
		super.start();
	}

	/**
	 * Update the class whenever a child has changed.
	 * Method is usually invoked from the children.
	 */
	public boolean update(Object event) {
		if (event == m_parmSurface) {
			return super.update(null);
		}
		return super.update(event);
	}
}

