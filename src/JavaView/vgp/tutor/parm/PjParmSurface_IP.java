package vgp.tutor.parm;

import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for PjParmSurface with panels for parametrized surfaces.
 * 
 * @see			jvx.surface.PgParmSurface_CP
 * @author		Konrad Polthier
 * @version		23.10.16, 1.20 revised (kp) Doc and notice added.<br>
 *					03.01.02, 1.10 revised (kp) Use control panel of PgParmSurface.<br>
 *					27.08.98, 1.00 created (kp) from PgParmSurface_IP.
 */
public class PjParmSurface_IP extends PjProject_IP {
	protected	PjParmSurface			m_pjParmSurface;
	/** Contains control panel of geometry. */
	protected	PsPanel					m_surfPanel;

	/** Constructor */
	public PjParmSurface_IP() {
		super();
		if (getClass() == PjParmSurface_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		m_surfPanel = new PsPanel();
		add(m_surfPanel);
	}		
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Template for parametrized surfaces, type your own formula.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjParmSurface = (PjParmSurface)parent;

		m_surfPanel.removeAll();
		{
			// Create an info panel of the workshop by ourself
			PsPanel cp = m_pjParmSurface.m_parmSurface.newInspector(PsPanel.CONFIG_EXT);
			cp.removeTitle();
			cp.setBorderType(PsPanel.BORDER_NONE);
			m_surfPanel.add(cp);
		}
	}
}
