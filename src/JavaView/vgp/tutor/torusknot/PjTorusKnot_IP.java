package vgp.tutor.torusknot;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jv.object.PsDebug;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.project.PjProject_IP;

/**
 * Info panel for with switches to display tube or surface.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 2.10 revised (kp) Doc and notice added.<br>
 *					01.06.03, 2.00 revised (kp) Handling of color and update improved.<br>
 *					02.03.01, 1.10 revised (kp) Moved to vgp.tutor from vgp.curve.<br>
 *					15.08.99, 1.10 revised (kp) Converted to AWT1.1 event model.<br>
 *					30.01.99, 1.00 created (kp)
 */
public class PjTorusKnot_IP extends PjProject_IP
	implements ActionListener, ItemListener {
	protected	PjTorusKnot				m_pjTorusKnot;

	protected	PsPanel					m_pBounds;
	protected	PsPanel					m_pTube;
	protected	Button					m_bReset;
	protected	Checkbox					m_cShowTorus;
	protected	Checkbox					m_cShowTube;
	protected	Checkbox					m_cShowKnot;

	/** Constructor */
	public PjTorusKnot_IP() {
		super();
		if (getClass() == PjTorusKnot_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());
		
		m_pBounds = new PsPanel();
		add(m_pBounds);
		
		addLine(1);

		m_pTube = new PsPanel();
		add(m_pTube);
		
		addLine(1);

		PsPanel pShow = new PsPanel(new GridLayout(1, 3));
		pShow.setInsetSize(3);
		add(pShow);
		{
			m_cShowTorus = new Checkbox("Show Torus");
			m_cShowTorus.addItemListener(this);
			pShow.add(m_cShowTorus);
			m_cShowTube = new Checkbox("Show Tube");
			m_cShowTube.addItemListener(this);
			pShow.add(m_cShowTube);
			m_cShowKnot = new Checkbox("Show Knot");
			m_cShowKnot.addItemListener(this);
			pShow.add(m_cShowKnot);
		}
		// buttons at bottom
		Panel pBottomButtons = new Panel(new FlowLayout(FlowLayout.CENTER));
		m_bReset = new Button ("Reset");
		m_bReset.addActionListener(this);
		pBottomButtons.add(m_bReset);
		add(pBottomButtons);
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Demo project for handling multiple geometries in the JavaView viewer. "+
							 "Here torus knots are computed and visualized additionally as thick tubes.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjTorusKnot = (PjTorusKnot)parent;
		m_pBounds.removeAll();
		m_pBounds.add(m_pjTorusKnot.m_numUWindings.newInspector(PsPanel.INFO_EXT));
		m_pBounds.add(m_pjTorusKnot.m_numZWindings.newInspector(PsPanel.INFO_EXT));
		m_pBounds.add(m_pjTorusKnot.m_polygonDiscr.newInspector(PsPanel.INFO_EXT));
		m_pBounds.add(m_pjTorusKnot.m_thickSlider.newInspector(PsPanel.INFO_EXT));
		m_pTube.add(m_pjTorusKnot.m_tube.newInspector(PsPanel.CONFIG_EXT));
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("isShowing = "+isShowing());
		if (event == m_project) {
			m_cShowTorus.setState(m_pjTorusKnot.m_bShowTorus);
			m_cShowTube.setState(m_pjTorusKnot.m_bShowTube);
			m_cShowKnot.setState(m_pjTorusKnot.m_bShowKnot);
			return true;
		}
		return super.update(event);
	}

	public void actionPerformed(ActionEvent event) {
		if (m_project==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			if (PsDebug.NOTIFY) PsDebug.notify("reset");
			m_pjTorusKnot.init();
			m_pjTorusKnot.update(m_pjTorusKnot);
			return;
		}
	}
	public void itemStateChanged(ItemEvent event) {
		if (m_project==null)
			return;
		Object source = event.getSource();
		if (source == m_cShowTorus) {
			if (PsDebug.NOTIFY) PsDebug.notify("switch show torus");
			m_pjTorusKnot.m_bShowTorus = m_cShowTorus.getState();
			if (m_pjTorusKnot.m_bShowTorus) {
				m_pjTorusKnot.addGeometry(m_pjTorusKnot.m_torus);
			} else {
				m_pjTorusKnot.removeGeometry(m_pjTorusKnot.m_torus);
			}
			return;
		} else if (source == m_cShowTube) {
			if (PsDebug.NOTIFY) PsDebug.notify("switch show tube");
			m_pjTorusKnot.m_bShowTube = m_cShowTube.getState();
			if (m_pjTorusKnot.m_bShowTube) {
				m_pjTorusKnot.addGeometry(m_pjTorusKnot.m_tube);
			} else {
				m_pjTorusKnot.removeGeometry(m_pjTorusKnot.m_tube);
			}
			return;
		} else if (source == m_cShowKnot) {
			if (PsDebug.NOTIFY) PsDebug.notify("switch show tube");
			m_pjTorusKnot.m_bShowKnot = m_cShowKnot.getState();
			if (m_pjTorusKnot.m_bShowKnot) {
				m_pjTorusKnot.addGeometry(m_pjTorusKnot.m_knot);
			} else {
				m_pjTorusKnot.removeGeometry(m_pjTorusKnot.m_knot);
			}
			return;
		}
	}
}

