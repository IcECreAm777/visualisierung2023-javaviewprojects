package vgp.tutor.torusknot;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Demo applet shows a torus knot on a surface, and demonstrates the use of multiple
 * geometries in the viewer.
 * 
 * @see			vgp.tutor.torusknot.PaTorusKnot
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				02.03.01, 1.10 revised (kp) Moved to vgp.tutor from vgp.curve.<br>
 *					30.01.99, 1.00 created (kp)
 */
public class PaTorusKnot extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Display a torus knot and show usage of multiple geometries in display" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjTorusKnot();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaTorusKnot(), args);
	}
}
