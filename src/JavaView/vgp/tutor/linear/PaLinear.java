package vgp.tutor.linear;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;

/**
 * Applet shows simple interactive linear algebra.
 * 
 * @see			vgp.tutor.linear.PjLinear
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				04.03.01, 1.10 revised (kp) Package moved to vgp.tutor.linear from dev.app.linear.<br>
 *					26.05.00, 1.00 created (kp) 
 */
public class PaLinear extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Applet shows simple interactive linear algebra" + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjLinear();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		PvApplet.main(new PaLinear(), args);
	}
}
