package vgp.tutor.tetraSet;

import jv.geom.PgTetraSet;
import jv.project.PvApplet;
import jv.project.PjProject;
import jv.project.PvDisplayIf;


/**
 * Generate and display a sample cubical grid as PgTetraSet.
 * 
 * @see			vgp.tutor.tetraSet.PaTetraSet
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				08.03.10, 1.10 revised (ur) Functionality moved to PwTetraSet Workshop.<br>
 * 				23.06.07, 1.00 created (kp)
 */
public class PaTetraSet extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright */
	public String getAppletInfo() {
		return "Name: " 		+ this.getClass().getName() + "\r\n" +
				 "Author: " 	+ "Konrad Polthier" + "\r\n" +
				 "Version: " 	+ "2.00" + "\r\n" +
				 "Applet shows refinement of tetrahedral volumes" + "\r\n";
	}
	/**
	 * Configure and initialize the viewer, load system and user projects.
	 * One of the user projects must be selected here.
	 */
	public void run() {
		super.run();

		drawMessage("Loading geometry ...");
		PgTetraSet m_tetra = new PgTetraSet(3); // create a new geometry
		m_tetra.setName("Cubical Volume");
		m_tetra.computeBox(3, 3, 3, -1., -1., -1., 1., 1., 1.);

		PvDisplayIf disp = super.getDisplay();
		disp.setEnabledZBuffer(true);
		disp.addGeometry(m_tetra);
		disp.selectGeometry(m_tetra);
		// disp.update(disp);
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return null;
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaTetraSet(), args);
	}
}
