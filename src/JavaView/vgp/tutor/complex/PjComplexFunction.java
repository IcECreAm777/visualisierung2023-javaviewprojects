package vgp.tutor.complex;

import java.awt.Color;

import jv.function.PuComplexFunction;
import jv.geom.PgElementSet;
import jv.number.PuComplex;
import jv.project.PjProject;
import jv.project.PvCameraIf;
import jv.project.PvDisplayIf;
import jv.project.PvViewerIf;
import jv.thirdParty.ruler.PgAxes;
import jv.vecmath.PdVector;
import jvx.surface.PgDomain;
import jvx.surface.PgDomainDescr;


/**
 * Tutorial project for parsing and handling of complex function expressions.
 * 
 * @see			jv.function.PuFunction
 * @author		Konrad Polthier
 * @version		29.08.08, 1.00 created (kp)
 */
public class PjComplexFunction extends PjProject {
	/** Domain in complex plane. */
	protected	PgDomain						m_domain;
	/** Image of complex map in complex plane. */
	protected	PgElementSet				m_image;
	/** String representation of function expressions. */
	protected	PuComplexFunction			m_function;
	/** Default function expressions. */
	protected	static String				m_defExpression	= "z*z";
	
	public PjComplexFunction() {
		super("Complex Function Parser Demo");
		m_domain				= new PgDomain(2);
		m_domain.setVisible(true);
		m_domain.setName("Domain of Graph");
		m_domain.setDimOfElements(4);
		m_domain.addUpdateListener(this);

		m_image			= new PgElementSet(2);

		m_function		= new PuComplexFunction(1, 1);
		m_function.setParent(this);

		if (getClass() == PjComplexFunction.class) {
			init();
		}
	}
	public void init() {
		super.init();

		PgDomainDescr descr = m_domain.getDescr();
		descr.setMaxSize(-10., -10., 10., 10.);
		descr.setSize(-2.,-2., 2., 2.);
		descr.setDiscrBounds(2, 2, 50, 50);
		descr.setDiscr(20, 20);
		m_domain.init();
		m_domain.setName("Complex Domain");
		m_domain.showElements(false);
		m_domain.makeVertexColorsFromXYZ();
		m_domain.showEdgeColorFromVertices(true);
		m_domain.showEdgeColors(true);
		m_domain.setGlobalEdgeColor(new Color(161, 161, 161));
		m_domain.setGlobalElementColor(Color.white);
		
		m_image.setName("Complex Map");
		m_image.copy(m_domain);
		m_image.setGlobalEdgeColor(Color.blue);

		m_function.setName("Complex Function");
		setExpression(m_defExpression);
	}
	public void start() {
		// Parse parameters from Html page or command line, if available
		PvViewerIf viewer = getViewer();
		if (viewer != null) {
			String sEquation = viewer.getParameter("dev.cfun.PjComplexFunction#expression");
			if (sEquation != null) {
				m_defExpression = sEquation;
				setExpression(m_defExpression);
			}
		}
		compute();

		addGeometry(m_domain);
		addGeometry(m_image);
		selectGeometry(m_domain);

		PvDisplayIf disp = getDisplay();
		if (disp != null) {
			disp.showAxes(true);
			PgAxes axes = ((jv.viewer.PvDisplay)disp).getAxes();
			axes.setMode(PgAxes.AXES_2DXY_CENTER);
			disp.selectCamera(PvCameraIf.CAMERA_ORTHO_XY);		// project onto xy-plane
		}
		super.start();
	}

	/**
	 * Update the class whenever a child has changed.
	 * Method is usually invoked from the children.
	 */
	public boolean update(Object event) {
		if (isUpdateSender())
			return true;
		if (event == this) {
			compute();
			setUpdateSender(true);
			{
				m_domain.update(m_domain);
				m_function.update(m_function);
				m_image.update(m_image);
			}
			setUpdateSender(false);
			return super.update(this);
		} else if (event == m_function) {
			compute();
			m_image.update(m_image);
			return super.update(this);
		} else if (event == m_domain) {
			m_domain.makeVertexColorsFromXYZ();
			m_image.copy(m_domain);
			m_image.setGlobalEdgeColor(Color.blue);
			compute();
			m_image.update(m_image);
			return super.update(this);
		}
		return false;
	}    

	public void setExpression(String eq) {
		m_function.setExpression(eq);
	}

	/**
	 * Perform all functional evaluations on vertex set of geometry.
	 * @version		07.08.08, 1.00 created (kp)
	 * @since		JavaView v.3.99.031
	 */
	public void compute() {
		int nov 					= m_domain.getNumVertices();
		PdVector [] vertex	= m_domain.getVertices();
		PuComplex src			= new PuComplex();
		PuComplex dest			= new PuComplex();
		for (int i=0; i<nov; i++) {
			src.set(vertex[i].m_data[0], vertex[i].m_data[1]);
			// Apply function.
			dest.copy(m_function.eval(src));
			m_image.setVertex(i, dest.re, dest.im);
		}
	}
}

