package vgp.tutor.complex;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import jv.function.PuComplexFunction;
import jv.object.PsConfig;
import jv.object.PsDebug;
import jv.object.PsObject;
import jv.object.PsPanel;
import jv.object.PsUpdateIf;
import jv.objectGui.PsTabPanel;
import jv.project.PjProject_IP;


/**
 * Info panel of tutorial for parsing and handling of complex functions.
 * 
 * @author		Konrad Polthier
 * @version		23.10.16, 1.10 revised (kp) Doc and notice added.<br>
 *					29.08.08, 1.00 created (kp)
 */
public class PjComplexFunction_IP extends PjProject_IP implements ActionListener {
	protected	PjComplexFunction			m_pjCFun;

	protected	PsTabPanel					m_tabPanel;
	protected	PsPanel						m_pFunPanel;
	protected	PsPanel						m_pFunButtons;
	protected	Button						m_bReset;

	/** Constructor */
	public PjComplexFunction_IP() {
		super();
		if (getClass() == PjComplexFunction_IP.class) {
			init();
		}
	}
	/** Provides layout and adds internal components. */
	public void init() {
		super.init();
		addTitle("");
		addNotice(getNotice());

		m_tabPanel = new PsTabPanel();
		add(m_tabPanel);

		m_pFunPanel		= new PsPanel();
		m_pFunButtons	= new PsPanel(new FlowLayout());
		{
			m_bReset = new Button(PsConfig.getMessage(true, 54000, "Reset All"));
			m_bReset.addActionListener(this);
			m_pFunButtons.add(m_bReset);
		}
	}
	/**
	 * Informational text on the usage of this project and its panel.
	 * This notice will be displayed if this info panel of the project is shown.
	 * The text is split at line breaks into individual lines on the dialog.
	 *
	 * @return		string with textual information about usage of project.
	 * @version		22.10.16, 1.00 created (kp)
	 * @since		JavaView 4.70.008
	 */
	public	String	getNotice() {
		String notice = "Tutorial for parsing and computing complex functions.";
		return notice;
	}
	/**
	 * Set parent of panel which supplies the data inspected by the panel.
	 */
	public void setParent(PsUpdateIf parent) {
		super.setParent(parent);
		setTitle(parent.getName());

		m_pjCFun = (PjComplexFunction)parent;
		
		m_tabPanel.init(); // reset tab
		// Create panels by ourself
		PsPanel domIP	= m_pjCFun.m_domain.newInspector(PsPanel.CONFIG_EXT);
		m_tabPanel.addPanel(PsConfig.getMessage(51073), domIP);

		// Functional transformation of geometry.
		m_pFunPanel.removeAll();
		{
			PuComplexFunction fun		= m_pjCFun.m_function;
			PsPanel funIP	= fun.newInspector(PsPanel.INFO_EXT);
			Enumeration sliderEnum = fun.getParameters();
			if (sliderEnum != null) {
				funIP.addSubTitle("Global Parameter");
				for ( ; sliderEnum.hasMoreElements(); ) {
					PsObject n = (PsObject)sliderEnum.nextElement();
					funIP.add(n.newInspector(PsPanel.INFO_EXT));
				}
			}
			m_pFunPanel.add(funIP);
			m_pFunPanel.add(m_pFunButtons);
		}
		m_tabPanel.addPanel(PsConfig.getMessage(51075), m_pFunPanel);
		
		m_tabPanel.setVisible(PsConfig.getMessage(51075));
		m_tabPanel.validate();
	}
	/**
	 * Update the panel whenever the parent has changed somewhere else.
	 * Method is invoked from the parent or its superclasses.
	 */
	public boolean update(Object event) {
		if (PsDebug.NOTIFY) PsDebug.notify("isShowing = "+isShowing());
		if (m_pjCFun == event) {
			return true;
		}
		return super.update(event);
	}
	/**
	 * Handle action events invoked from buttons, menu items, text fields.
	 */
	public void actionPerformed(ActionEvent event) {
		if (m_pjCFun==null)
			return;
		Object source = event.getSource();
		if (source == m_bReset) {
			m_pjCFun.init();
			m_pjCFun.update(m_pjCFun);
		}
	}
}
