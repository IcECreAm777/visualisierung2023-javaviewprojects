package vgp.tutor.complex;

import java.awt.Rectangle;

import jv.project.PvApplet;
import jv.project.PjProject;


/**
 * Tutorial applet for parsing and handling of complex functions.
 * 
 * @see			jv.function.PuFunction
 * @author		Konrad Polthier
 * @version		08.11.16, 2.00 revised (kp) Full rewrite, superclass changed to PvApplet.<br>
 * 				29.08.08, 1.00 created (kp)
 */
public class PaComplexFunction extends PvApplet {
	/** Interface of applet to inform about author, version, and copyright. */
	public String getAppletInfo() {
		return "Name: "		+ this.getClass().getName()+ "\r\n" +
				 "Author: "		+ "Konrad Polthier" + "\r\n" +
				 "Version: "	+ "2.00" + "\r\n" +
				 "Tutorial applet for parsing and handling of complex functions." + "\r\n";
	}
	/** Return the preferred size of application frame. */
	public Rectangle getSizeOfFrame() {
		return super.getSizeOfFrame();
	}
	/** Return a new allocated project instance. */
	public PjProject getProject() {
		return new PjComplexFunction();
	}
	/**
	 * Standalone application support. The main() method acts as the applet's
	 * entry point when it is run as a standalone application. It is ignored
	 * if the applet is run from within an HTML page.
	 */
	public static void main(String args[]) {
		main(new PaComplexFunction(), args);
	}
}
