@echo off
rem # USAGE:    Run
rem #             javaview.bat [options] [geometry-file]
rem #           from the windows explorer or command line.
rem #           - the geometry-file is optional
rem #           - use a relative path name to the geometry file
rem #           - each option pair must be enclosed in quotes like "key=value"
rem #
rem # EXAMPLES: Switch to base of your JavaView installation
rem #             cd c:\Programme\JavaView\
rem #           Then run JavaView with a command like
rem #             bin\javaview.bat sample.obj
rem #           or include options such as
rem #             bin\javaview.bat "control=show" "panel=material" sample.obj
rem #             bin\javaview.bat "language=de" sample.obj
rem #
rem # NOTES:    - You may need to edit the variables below
rem #           - Optionally, put a link to this BAT file on your desktop,
rem #             and in the property panel of the link adjust the directory "Start in .."
rem #             to the JavaView base directory such as "c:\program files\JavaView\".
rem # 
rem # REQUIRED: It is necessary that Java is installed on your computer. Otherwise
rem #           download and install the latest Java version from Oracle (www.java.com).
rem #
rem # PURPOSE:  Launch JavaView, optionally with a 3d-geometry and command line args
rem #
rem # VERSION:  02.07, 2018-02-27 (kp) Default memory incremented to 1MB (i.e. using Xmx1024m instead of Xmx512).
rem #           02.06, 2015-03-29 (kp) Trailing path separator appended to codebase, for cosmetic reasons.
rem #           02.05, 2013-04-14 (kp) Comments updated.
rem #           02.02, 2007-02-12 (fk) Use javaw.exe instead of java.exe to avoid console window.
rem #           02.01, 2007-01-02 (fk) Codebase set to parent directory of this file and doc adjusted.
rem #           02.00, 2006-11-12 (kp) Support for Microsoft Java and Win98 dropped.
rem #

rem # SET CODEBASE (no need to edit):
rem # 
rem # The default JavaView codebase folder is the parent directory of this file.
set jv_cb="%~dp0.."

rem # SET JAR LIBRARIES (optionally edit):
rem # 
rem # The variable jv_jar specifies the location of the JavaView archive "javaview.jar"
rem # The two archives "jvx.jar" and "vgpapp.jar" are optional.
set jv_jar=%jv_cb%/jars/javaview.jar;%jv_cb%/jars/jvx.jar;%jv_cb%/jars/vgpapp.jar;%jv_cb%

rem # Append trailing path separator to codebase
set jv_cb=%jv_cb%\

rem # LAUNCH IN NORMAL MODE:
rem # 
rem # Launch JavaView, should open a 3d display.
rem # It is assumed that Java is installed on your computer. Otherwise
rem # download and install the latest Java version from http://www.java.com.
rem # Here Java's default memory size is increased to 1GB using the -Xmx parameter.
rem # start javaw -cp %jv_jar% -Xmx512m javaview codebase=%jv_cb% %*
start javaw -cp %jv_jar% -Xmx1024m javaview codebase=%jv_cb% %*

rem # LAUNCH IN ALTERNATE MODE with more memory:
rem # 
rem # The following alternative command increases the memory available to the Java virtual machine
rem # and should be used if Java runs out of memory during a JavaView session.
rem # Here Java's default memory size is increased to 6GB. Adjust to your hardware capacitiy.
rem # start javaw -cp %jv_jar% -Xmx6144m javaview codebase=%jv_cb% %*